%define MDOC
"class WarpIVExtent
Index space representation of a cartesian volume.

Represnetation of a cartesian volume and common operations
on it. The implementation is intended to be fast and light
so that it may be used in place of int[6] with little or no
performance penalty.
"
%enddef

%module (docstring=MDOC) WarpIVExtent

%{
#include "WarpIVExtent.h"
#include <sstream>
%}

%ignore operator<<(std::ostream &, const WarpIVExtent &);
%ignore WarpIVExtent::operator[](int);
%ignore WarpIVExtent::operator[](int) const;
%ignore WarpIVExtent::operator=(const WarpIVExtent &);
%ignore WarpIVExtent::Size(const WarpIVExtent &);
%ignore WarpIVExtent::Grow(WarpIVExtent const &,int,int);
%ignore WarpIVExtent::Shrink(WarpIVExtent const &,int,int);
%ignore WarpIVExtent::GetLowerBound(WarpIVExtent const &,double const [3],double const [3],double [3]);
%ignore WarpIVExtent::GetLowerBound(WarpIVExtent const &,float const *,float const *,float const *,double [3]);

%include "WarpIVExtent.h"

%extend WarpIVExtent
{
    const char *__str__()
    {
        static std::ostringstream oss;
        oss.str("");
        oss << *self;
        return oss.str().c_str();
    }

    int __getitem__(int i)
    {
        i = i % 6;
        if (i < 0) i = 6 + i;
        return $self->operator[](i);
    }

    void __setitem__(int i, int v)
    {
        i = i % 6;
        if (i < 0) i = 6 + i;
        $self->operator[](i) = v;
    }

    void _iand(const WarpIVExtent &other)
    {
        $self->operator&=(other);
    }

%pythoncode
%{
def __iand__(self, other):
    """ """
    self._iand(other)
    return self
%}
}
