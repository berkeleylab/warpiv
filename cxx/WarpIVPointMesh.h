#ifndef WarpIVPointMesh_h
#define WarpIVPointMesh_h

#include "WarpIVMeshId.h"

#include <limits>
#include <cstring>

// **************************************************************************
template<typename T>
void meshIds(
    // point set
    const T * __restrict__ x, const T * __restrict__ z, mesh_id_t npts,
    // origin, spacing and local extent
    T x0, T z0, T dx, T dz, mesh_id_t nxc, mesh_id_t nzc,
    // mesh ids that each point falls in
    mesh_id_t * __restrict__ ids)
{
    (void)nzc;
    T dxi = T(1)/dx;
    T dzi = T(1)/dz;
    #pragma ivdep
    for (mesh_id_t q = 0; q < npts; ++q)
    {
        ids[q] = static_cast<mesh_id_t>((z[q] - z0)*dzi)*nxc
            + static_cast<mesh_id_t>((x[q] - x0)*dxi);
    }
}

// **************************************************************************
template<typename T>
void meshIds(
    // point set
    const T * __restrict__ x,
    const T * __restrict__ y,
    const T * __restrict__ z,
    mesh_id_t npts,
    // origin, spacing and local extent
    T x0, T y0, T z0,
    T dx, T dy, T dz,
    mesh_id_t nxc, mesh_id_t nyc, mesh_id_t nzc,
    // mesh ids that each point falls in
    mesh_id_t * __restrict__ ids)
{
    (void)nzc;
    T dxi = T(1)/dx;
    T dyi = T(1)/dy;
    T dzi = T(1)/dz;
    mesh_id_t nxyc = nxc*nyc;
    #pragma ivdep
    for (mesh_id_t q = 0; q < npts; ++q)
    {
        ids[q] = static_cast<mesh_id_t>((z[q] - z0)*dzi)*nxyc
            + static_cast<mesh_id_t>((y[q] - y0)*dyi)*nxc
            + static_cast<mesh_id_t>((x[q] - x0)*dxi);
    }
}

// **************************************************************************
template<typename T>
void meshDensity(
    // mesh ids that each point falls in
    const mesh_id_t * __restrict__ ids,
    mesh_id_t npts,
    mesh_id_t nc,
    // computed density
    T * __restrict__ den)
{
    memset(den, 0, nc*sizeof(T));
    for (mesh_id_t q = 0; q < npts; ++q)
        den[ids[q]] += T(1);
}

// **************************************************************************
template<typename T>
void meshAverage(
    // array to average
    const T * __restrict__ ar,
    // mesh ids that each point falls in
    const mesh_id_t * __restrict__ ids,
    mesh_id_t npts,
    mesh_id_t nc,
    // pre computed density, and length of array
    const T * __restrict__ den,
    // result
    T * __restrict__ avg)
{
    memset(avg, 0, nc*sizeof(T));

    for (mesh_id_t q = 0; q < npts; ++q)
        avg[ids[q]] += ar[q];

    #pragma ivdep
    for (mesh_id_t q = 0; q < nc; ++q)
        avg[q] /= den[q] ? den[q] : T(1);
}

// **************************************************************************
template<typename T>
void meshMin(
    // array to min
    const T * __restrict__ ar,
    // mesh ids that each point falls in
    const mesh_id_t * __restrict__ ids,
    mesh_id_t npts,
    mesh_id_t nc,
    // computed density
    T * __restrict__ mn)
{
    T ival = std::numeric_limits<T>::max();
    #pragma ivdep
    for (mesh_id_t q = 0; q < nc; ++q)
        mn[q] = ival;

    for (mesh_id_t q = 0; q < npts; ++q)
    {
        mesh_id_t qq = ids[q];
        mn[qq] = ar[q] < mn[qq] ?  ar[q] : mn[qq];
    }

    #pragma ivdep
    for (mesh_id_t q = 0; q < nc; ++q)
        mn[q] = mn[q] == ival ? 0.0 : mn[q];
}

// **************************************************************************
template<typename T>
void meshMax(
    // array to max
    const T * __restrict__ ar,
    // mesh ids that each point falls in
    const mesh_id_t * __restrict__ ids,
    mesh_id_t npts,
    mesh_id_t nc,
    // computed density
    T * __restrict__ mx)
{
    T ival = -std::numeric_limits<T>::max();
    #pragma ivdep
    for (mesh_id_t q = 0; q < nc; ++q)
        mx[q] = ival;

    for (mesh_id_t q = 0; q < npts; ++q)
    {
        mesh_id_t qq = ids[q];
        mx[qq] = ar[q] > mx[qq] ?  ar[q] : mx[qq];
    }

    #pragma ivdep
    for (mesh_id_t q = 0; q < nc; ++q)
        mx[q] = mx[q] == ival ? 0.0 : mx[q];
}

#endif
