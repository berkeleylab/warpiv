#!/usr/bin/env python
from distutils.core import setup, Extension

relFlags = ['-std=c++11', '-O3', '-mavx', '-ffast-math', \
        '-Wall', '-Wextra', '-Wno-unknown-pragmas', '-Wno-unused-local-typedefs',  \
        '-fopt-info-optimized']

debFlags = ['-std=c++11', '-O', '-g', '-Wall', '-Wextra', \
        '-Wno-unknown-pragmas', '-Wno-unused-local-typedefs']

cxxFlags = relFlags

pointMesh_mod = Extension( \
    '_pointMesh', sources = ['pointMeshSwig.cxx'],
    extra_compile_args = cxxFlags,
    )

setup(name = 'pointMesh', version = '1.0', author = "Burlen Loring",
    description = """Functions for moving points to a mesh""",
    ext_modules = [pointMesh_mod], py_modules = ["pointMesh"],
    )


yeeCell_mod = Extension( \
    '_yeeCell', sources=['yeeCellWrap.cxx','yeeCellSwig.cxx'],
    extra_compile_args= cxxFlags,
    )

setup(name = 'yeeCell', version = '1.0', author = "Burlen Loring",
    description = """Functions for converting from Yee centering""",
    ext_modules = [yeeCell_mod], py_modules = ["yeeCell"],
    )
