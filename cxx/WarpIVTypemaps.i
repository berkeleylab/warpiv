/* basic typemaps for dealing with numpy */
%typemap(in) PyArrayObject * {
    // verify that we are given numpy arrays
    if (pyArrayObjectCast($input, $1))
    {
        PyErr_Format(PyExc_ValueError,
            "Not a numpy array in argument %d of %s", $argnum, "$1_name");
        return NULL;
    }
}

%typemap(out) PyArrayObject * {
    $result = reinterpret_cast<PyObject*>($1);
}
