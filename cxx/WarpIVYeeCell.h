#ifndef WarpIVYeeCell_h
#define WarpIVYeeCell_h

#include "WarpIVMeshId.h"

// em.l_2dxz case Ex, Ez and Bx, Bz on are edges. Ey is on
// nodes, By is on centers and J has same layout as E
// arrays all have size n_q = nv_q + 2*ng_q + 1 where q is
// one of x,y,z

// **************************************************************************
template<typename T>
void cpy(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // copy from valid values
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        T *ao = &aout[k*nxo];
        T *ai = &ain[(k + ngz)*nxi + ngx];
        #pragma ivdep
        for (mesh_id_t i = 0; i < nxo; ++i)
            ao[i] = ai[i];
    }
}

// **************************************************************************
template<typename T>
void xAvg(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // average 2 values in the x direction
    T dxi = 0.5;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        T *ao = &aout[k*nxo];
        T *
        ai = &ain[(k + ngz)*nxi + ngx];
        #pragma ivdep
        for (mesh_id_t i = 0; i < nxo; ++i)
            ao[i] = dxi*ai[i];

        ai = &ain[(k + ngz)*nxi + ngx - 1];
        #pragma ivdep
        for (mesh_id_t i = 0; i < nxo; ++i)
            ao[i] += dxi*ai[i];
    }
}

// **************************************************************************
template<typename T>
void zAvg(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // average 2 values in the z direction
    T dzi = 0.5;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        T *ao = &aout[k*nxo];
        T *
        ai = &ain[(k + ngz)*nxi + ngx];
        #pragma ivdep
        for (mesh_id_t i = 0; i < nxo; ++i)
            ao[i] = dzi*ai[i];

        ai = &ain[(k + ngz - 1)*nxi + ngx];
        #pragma ivdep
        for (mesh_id_t i = 0; i < nxo; ++i)
            ao[i] += dzi*ai[i];
    }
}


// **************************************************************************
template<typename T>
void xzAvg(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // average of 4 surounding values
    T dxzi = 0.25;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        T *ao = &aout[k*nxo];
        T *
        ai = &ain[(k + ngz    )*nxi + ngx    ];
        #pragma ivdep
        for (mesh_id_t i = 0; i < nxo; ++i)
            ao[i] = dxzi*ai[i];

        ai = &ain[(k + ngz    )*nxi + ngx - 1];
        #pragma ivdep
        for (mesh_id_t i = 0; i < nxo; ++i)
            ao[i] += dxzi*ai[i];

        ai = &ain[(k + ngz - 1)*nxi + ngx - 1];
        #pragma ivdep
        for (mesh_id_t i = 0; i < nxo; ++i)
            ao[i] += dxzi*ai[i];

        ai = &ain[(k + ngz - 1)*nxi + ngx    ];
        #pragma ivdep
        for (mesh_id_t i = 0; i < nxo; ++i)
            ao[i] += dxzi*ai[i];
    }
}

// 3d case, E and J are on edges B is on faces
// arrays all have size n_q = nv_q + 2*ng_q + 1 where q is
// one of x,y,z

// **************************************************************************
template<typename T>
void cpy(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // copy from valid values
    mesh_id_t nxyo = nxo*nyo;
    mesh_id_t nxyi = nxi*nyi;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        for (mesh_id_t j = 0; j < nyo; ++j)
        {
            T *ao = &aout[k*nxyo + j*nxo];
            T *ai = &ain[(k + ngz)*nxyi + (j + ngy)*nxi + ngx];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] = ai[i];
        }
    }
}

// **************************************************************************
template<typename T>
void xAvg(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // average 2 values in the x direction
    mesh_id_t nxyo = nxo*nyo;
    mesh_id_t nxyi = nxi*nyi;
    T dxi = 0.5;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        for (mesh_id_t j = 0; j < nyo; ++j)
        {
            T *ao = &aout[k*nxyo + j*nxo];
            T *
            ai = &ain[(k + ngz)*nxyi + (j + ngy)*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] = dxi*ai[i];

            ai = &ain[(k + ngz)*nxyi + (j + ngy)*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxi*ai[i];
        }
    }
}

// **************************************************************************
template<typename T>
void yAvg(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // average 2 values in the y direction
    mesh_id_t nxyo = nxo*nyo;
    mesh_id_t nxyi = nxi*nyi;
    T dxi = 0.5;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        for (mesh_id_t j = 0; j < nyo; ++j)
        {
            T *ao = &aout[k*nxyo + j*nxo];
            T *
            ai = &ain[(k + ngz)*nxyi + (j + ngy    )*nxi + ngx];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] = dxi*ai[i];

            ai = &ain[(k + ngz)*nxyi + (j + ngy - 1)*nxi + ngx];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxi*ai[i];
        }
    }
}

// **************************************************************************
template<typename T>
void zAvg(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // average 2 values in the x direction
    mesh_id_t nxyo = nxo*nyo;
    mesh_id_t nxyi = nxi*nyi;
    T dxi = 0.5;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        for (mesh_id_t j = 0; j < nyo; ++j)
        {
            T *ao = &aout[k*nxyo + j*nxo];
            T *
            ai = &ain[(k + ngz    )*nxyi + (j + ngy)*nxi + ngx];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] = dxi*ai[i];

            ai = &ain[(k + ngz - 1)*nxyi + (j + ngy)*nxi + ngx];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxi*ai[i];
        }
    }
}

// **************************************************************************
template<typename T>
void xyAvg(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // average of 4 surounding values
    mesh_id_t nxyo = nxo*nyo;
    mesh_id_t nxyi = nxi*nyi;
    T dxyi = 0.25;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        for (mesh_id_t j = 0; j < nyo; ++j)
        {
            T *ao = &aout[k*nxyo + j*nxo];
            T *
            ai = &ain[(k + ngz)*nxyi + (j + ngy    )*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] = dxyi*ai[i];

            ai = &ain[(k + ngz)*nxyi + (j + ngy    )*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxyi*ai[i];

            ai = &ain[(k + ngz)*nxyi + (j + ngy - 1)*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxyi*ai[i];

            ai = &ain[(k + ngz)*nxyi + (j + ngy - 1)*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxyi*ai[i];
        }
    }
}

// **************************************************************************
template<typename T>
void xzAvg(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // average of 4 surounding values
    mesh_id_t nxyo = nxo*nyo;
    mesh_id_t nxyi = nxi*nyi;
    T dxzi = 0.25;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        for (mesh_id_t j = 0; j < nyo; ++j)
        {
            T *ao = &aout[k*nxyo + j*nxo];
            T *
            ai = &ain[(k + ngz    )*nxyi + (j + ngy)*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] = dxzi*ai[i];

            ai = &ain[(k + ngz    )*nxyi + (j + ngy)*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxzi*ai[i];

            ai = &ain[(k + ngz - 1)*nxyi + (j + ngy)*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxzi*ai[i];

            ai = &ain[(k + ngz - 1)*nxyi + (j + ngy)*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxzi*ai[i];
        }
    }
}

// **************************************************************************
template<typename T>
void yzAvg(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // average of 4 surounding values
    mesh_id_t nxyo = nxo*nyo;
    mesh_id_t nxyi = nxi*nyi;
    T dyzi = 0.25;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        for (mesh_id_t j = 0; j < nyo; ++j)
        {
            T *ao = &aout[k*nxyo + j*nxo];
            T *
            ai = &ain[(k + ngz)*nxyi + (j + ngy    )*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] = dyzi*ai[i];

            ai = &ain[(k + ngz)*nxyi + (j + ngy    )*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dyzi*ai[i];

            ai = &ain[(k + ngz)*nxyi + (j + ngy - 1)*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dyzi*ai[i];

            ai = &ain[(k + ngz)*nxyi + (j + ngy - 1)*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dyzi*ai[i];
        }
    }
}

// **************************************************************************
template<typename T>
void xyzAvg(
    // input Yee centered field
    T * __restrict__ ain,
    // input mesh dims
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,
    // output mesh dims
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,
    // num ghost zones
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz,
    // output node centered field
    T * __restrict__ aout)
{
    (void)nzi;
    // average of 8 surounding values
    mesh_id_t nxyo = nxo*nyo;
    mesh_id_t nxyi = nxi*nyi;
    T dxyzi = 0.125;
    for (mesh_id_t k = 0; k < nzo; ++k)
    {
        for (mesh_id_t j = 0; j < nyo; ++j)
        {
            T *ao = &aout[k*nxyo + j*nxo];
            T *
            ai = &ain[(k + ngz    )*nxyi + (j + ngy    )*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] = dxyzi*ai[i];

            ai = &ain[(k + ngz    )*nxyi + (j + ngy    )*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxyzi*ai[i];

            ai = &ain[(k + ngz    )*nxyi + (j + ngy - 1)*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxyzi*ai[i];

            ai = &ain[(k + ngz    )*nxyi + (j + ngy - 1)*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxyzi*ai[i];

            ai = &ain[(k + ngz - 1)*nxyi + (j + ngy    )*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] = dxyzi*ai[i];

            ai = &ain[(k + ngz - 1)*nxyi + (j + ngy    )*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxyzi*ai[i];

            ai = &ain[(k + ngz - 1)*nxyi + (j + ngy - 1)*nxi + ngx - 1];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxyzi*ai[i];

            ai = &ain[(k + ngz - 1)*nxyi + (j + ngy - 1)*nxi + ngx    ];
            #pragma ivdep
            for (mesh_id_t i = 0; i < nxo; ++i)
                ao[i] += dxyzi*ai[i];
        }
    }
}

#endif
