#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#define PY_ARRAY_UNIQUE_SYMBOL  PyArray_API_yeeCell
#define NO_IMPORT_ARRAY
#include <numpy/arrayobject.h>
#include "WarpIVGlue.h"
#include "WarpIVYeeCell.h"
#include "WarpIVYeeCellWrap.h"

#define d2_conversion_wrapper_impl(_py_name, _c_name)                               \
PyArrayObject *_py_name(                                                            \
    PyArrayObject *ain,                                                             \
    mesh_id_t nxi, mesh_id_t nzi,                                                   \
    mesh_id_t nxo, mesh_id_t nzo,                                                   \
    mesh_id_t ngx, mesh_id_t ngz)                                                   \
{                                                                                   \
    mesh_id_t ncells = nxo*nzo;                                                     \
                                                                                    \
    /* dispatch based on input type */                                              \
    PyArrayObject *aout = NULL;                                                     \
    switch (getType(ain))                                                           \
    {                                                                               \
        numpyDispatch(                                                              \
            CPP_TT::Type *in = NULL;                                                \
            if (getCPointer(ain, in))                                               \
            {                                                                       \
                PyErr_Format(PyExc_ValueError,                                      \
                    "failed to get pointer to input");                              \
                return NULL;                                                        \
            }                                                                       \
                                                                                    \
            aout = allocatePyArray<CPP_TT::Type>(ncells);                           \
            CPP_TT::Type *out = static_cast<CPP_TT::Type*>(PyArray_DATA(aout));     \
                                                                                    \
            _c_name(in, nxi, nzi, nxo, nzo, ngx, ngz, out);                         \
            )                                                                       \
        default:                                                                    \
            PyErr_Format(PyExc_ValueError, "failed dispatch unsupported type");     \
            return NULL;                                                            \
    }                                                                               \
    return aout;                                                                    \
}

#define d3_conversion_wrapper_impl(_py_name, _c_name)                               \
PyArrayObject *_py_name(                                                            \
    PyArrayObject *ain,                                                             \
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,                                    \
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,                                    \
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz)                                    \
{                                                                                   \
    mesh_id_t ncells = nxo*nyo*nzo;                                                 \
                                                                                    \
    /* dispatch based on input type */                                              \
    PyArrayObject *aout = NULL;                                                     \
    switch (getType(ain))                                                           \
    {                                                                               \
        numpyDispatch(                                                              \
            CPP_TT::Type *in = NULL;                                                \
            if (getCPointer(ain, in))                                               \
            {                                                                       \
                PyErr_Format(PyExc_ValueError,                                      \
                    "failed to get pointer to input");                              \
                return NULL;                                                        \
            }                                                                       \
                                                                                    \
            aout = allocatePyArray<CPP_TT::Type>(ncells);                           \
            CPP_TT::Type *out = static_cast<CPP_TT::Type*>(PyArray_DATA(aout));     \
                                                                                    \
            _c_name(in, nxi, nyi, nzi, nxo, nyo, nzo, ngx, ngy, ngz, out);          \
            )                                                                       \
        default:                                                                    \
            PyErr_Format(PyExc_ValueError, "failed dispatch unsupported type");     \
            return NULL;                                                            \
    }                                                                               \
    return aout;                                                                    \
}

d2_conversion_wrapper_impl(cpy2, cpy)
d2_conversion_wrapper_impl(xAvg2, xAvg)
d2_conversion_wrapper_impl(zAvg2, zAvg)
d2_conversion_wrapper_impl(xzAvg2, xzAvg)

d3_conversion_wrapper_impl(cpy3, cpy)
d3_conversion_wrapper_impl(xAvg3, xAvg)
d3_conversion_wrapper_impl(yAvg3, yAvg)
d3_conversion_wrapper_impl(zAvg3, zAvg)
d3_conversion_wrapper_impl(xyAvg3, xyAvg)
d3_conversion_wrapper_impl(xzAvg3, xzAvg)
d3_conversion_wrapper_impl(yzAvg3, yzAvg)
d3_conversion_wrapper_impl(xyzAvg3, xyzAvg)
