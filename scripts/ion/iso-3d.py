
try:
    i3_it
except NameError:
    i3_it = 0
else:
    i3_it += 1

spec = ['Electron-All', 'Carbon-0', 'Proton-0']
#expr = ['e_log_ke', 'c_log_ke', 'p_log_ke']
expr = ['Electron-All/ke', 'Carbon-0/ke', 'Proton-0/ke']
op_out = ['den', 'ke']

i = 0
while i < 3:
    active_spec = spec[i]
    active_expr = expr[i]
    j = 0
    while j < 2:
        # this ensures a consistent axes
        AddPlot("Pseudocolor", "grid/E_magnitude", 1, 0)
        DrawPlots()
        HideActivePlots()

        # Use data binning
        if dataBin:
            # add a var that exists
            AddPlot("Pseudocolor", active_spec + "/e_magnitude", 1, 0)

            AddOperator("DataBinning",0)

            # fix variable
            ChangeActivePlotsVar("operators/DataBinning/3D/" + active_spec)

            DataBinningAtts = DataBinningAttributes()

            op = [DataBinningAtts.Count, DataBinningAtts.Average]
            active_op = op[j]

            DataBinningAtts.numDimensions = DataBinningAtts.Three  # One, Two, Three
            DataBinningAtts.dim1BinBasedOn = DataBinningAtts.X  # X, Y, Z, Variable
            DataBinningAtts.dim1NumBins = int(nx/dim_scale)
            DataBinningAtts.dim2BinBasedOn = DataBinningAtts.Y  # X, Y, Z, Variable
            DataBinningAtts.dim2NumBins = int(ny/dim_scale)
            DataBinningAtts.dim3BinBasedOn = DataBinningAtts.Z  # X, Y, Z, Variable
            DataBinningAtts.dim3NumBins = int(nz/dim_scale)
            DataBinningAtts.reductionOperator = active_op  # Average, Minimum, Maximum, StandardDeviation, Variance, Sum, Count, RMS, PDF
            DataBinningAtts.varForReduction = active_expr
            DataBinningAtts.emptyVal = 0
            DataBinningAtts.outputType = DataBinningAtts.OutputOnBins  # OutputOnBins, OutputOnInputMesh
            SetOperatorOptions(DataBinningAtts, 0)
        else:
            # Use the data binning computed by WarpIV
            if j==0:
                AddPlot("Pseudocolor", "grid/" + active_spec + "/den")
            elif j==1:
                AddPlot("Pseudocolor", "grid/" + active_spec + "/avg/ke")

        AddOperator("DualMesh", 0)

        AddOperator("Isosurface", 0)
        IsosurfaceAtts = IsosurfaceAttributes()
        IsosurfaceAtts.contourNLevels = 25
        IsosurfaceAtts.contourValue = ()
        IsosurfaceAtts.contourPercent = ()
        IsosurfaceAtts.contourMethod = IsosurfaceAtts.Level  # Level, Value, Percent
        IsosurfaceAtts.minFlag = 0
        IsosurfaceAtts.min = 0
        IsosurfaceAtts.maxFlag = 0
        IsosurfaceAtts.max = 100
        IsosurfaceAtts.scaling = IsosurfaceAtts.Linear  # Linear, Log
        IsosurfaceAtts.variable = "default"
        SetOperatorOptions(IsosurfaceAtts, 0)

        PseudocolorAtts = PseudocolorAttributes()
        PseudocolorAtts.minFlag = 1
        PseudocolorAtts.min = 0
        PseudocolorAtts.maxFlag = 0
        PseudocolorAtts.max = 100
        PseudocolorAtts.colorTableName = "hot"
        PseudocolorAtts.invertColorTable = 0
        PseudocolorAtts.opacityType = PseudocolorAtts.ColorTable  if op_out[j] == 'den' else PseudocolorAtts.Ramp # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
        PseudocolorAtts.opacity = 0.5 if active_spec == 'Electron-All' else 1.0
        PseudocolorAtts.opacity = 0.1
        PseudocolorAtts.opacityVarMin = 0
        PseudocolorAtts.opacityVarMax = 1
        PseudocolorAtts.opacityVarMinFlag = 0
        PseudocolorAtts.opacityVarMaxFlag = 0
        SetPlotOptions(PseudocolorAtts)

        DrawPlots()
        setCam()
        setAtts()

        DrawPlots()
        saveWin('%s-iso-%s-3d-%04d.png'%(active_spec, op_out[j], i3_it))

        DeleteAllPlots()

        j += 1
    i += 1
