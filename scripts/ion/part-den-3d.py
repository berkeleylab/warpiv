
var = ['Electron-All', 'Carbon-0', 'Proton-0']

try:
    pd3_it
except NameError:
    pd3_it = 0
else:
    pd3_it += 1

for active_var in var:
    # this ensures a consistent axes
    AddPlot("Mesh", "grid", 1, 0)
    DrawPlots()
    HideActivePlots()

    # density computed by data binning doesn't
    # yet exist.
    AddPlot("Pseudocolor", active_var + "/e_magnitude", 1, 0)

    AddOperator("DataBinning",0)
    DataBinningAtts = DataBinningAttributes()
    DataBinningAtts.numDimensions = DataBinningAtts.Three  # One, Two, Three
    DataBinningAtts.dim1BinBasedOn = DataBinningAtts.X  # X, Y, Z, Variable
    DataBinningAtts.dim1NumBins = int(nx)
    DataBinningAtts.dim2BinBasedOn = DataBinningAtts.Y  # X, Y, Z, Variable
    DataBinningAtts.dim2NumBins = int(ny)
    DataBinningAtts.dim3BinBasedOn = DataBinningAtts.Z  # X, Y, Z, Variable
    DataBinningAtts.dim3NumBins = int(nz)
    DataBinningAtts.reductionOperator = DataBinningAtts.Count  # Average, Minimum, Maximum, StandardDeviation, Variance, Sum, Count, RMS, PDF
    DataBinningAtts.emptyVal = 0
    DataBinningAtts.outputType = DataBinningAtts.OutputOnInputMesh  # OutputOnBins, OutputOnInputMesh
    SetOperatorOptions(DataBinningAtts, 0)

    # fix pcolor variable
    ChangeActivePlotsVar("operators/DataBinning/3D/" + active_var)

    PseudocolorAtts = PseudocolorAttributes()
    PseudocolorAtts.limitsMode = PseudocolorAtts.OriginalData  # OriginalData, CurrentPlot
    PseudocolorAtts.minFlag = 1
    PseudocolorAtts.min = 0
    PseudocolorAtts.maxFlag = 0
    PseudocolorAtts.max = 1
    PseudocolorAtts.colorTableName = "hot"
    PseudocolorAtts.invertColorTable = 0
    PseudocolorAtts.opacityType = PseudocolorAtts.Ramp  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
    PseudocolorAtts.opacityVariable = ""
    PseudocolorAtts.opacity = 1.0
    PseudocolorAtts.pointSize = 0.05
    PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
    PseudocolorAtts.pointSizePixels = 1
    SetPlotOptions(PseudocolorAtts)

    DrawPlots()
    setCam()
    setAtts()
    DrawPlots()

    saveWin('%s-part-den-3d-%04d.png'%(active_var, pd3_it))
    #recordStats(active_var + '(part den 3d)')

    DeleteAllPlots()
