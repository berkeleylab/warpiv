
var = ['Electron-All', 'Carbon-0', 'Proton-0']
nvar = len(var)

# counter for cycling various script params
try:
    pu3_it
except NameError:
    pu3_it = 0
    pu3_im_id = 0
else:
    pu3_it += 1
    if pu3_it%nvar == 0:
        pu3_im_id += 1

active_var = var[pu3_it%len(var)]

AddPlot("Mesh", "grid", 1, 0)
DrawPlots()
HideActivePlots()

AddPlot("Pseudocolor", active_var + "/u_magnitude", 1, 0)
PseudocolorAtts = PseudocolorAttributes()
PseudocolorAtts.scaling = PseudocolorAtts.Linear  # Linear, Log, Skew
PseudocolorAtts.skewFactor = 1
PseudocolorAtts.limitsMode = PseudocolorAtts.OriginalData  # OriginalData, CurrentPlot
PseudocolorAtts.minFlag = 1
PseudocolorAtts.min = 0
PseudocolorAtts.maxFlag = 0
PseudocolorAtts.max = 1
PseudocolorAtts.centering = PseudocolorAtts.Natural  # Natural, Nodal, Zonal
PseudocolorAtts.colorTableName = "hot"
PseudocolorAtts.invertColorTable = 0
PseudocolorAtts.opacityType = PseudocolorAtts.Constant  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
PseudocolorAtts.opacityVariable = ""
PseudocolorAtts.opacity = 0.25
PseudocolorAtts.opacityVarMin = 0
PseudocolorAtts.opacityVarMax = 1
PseudocolorAtts.opacityVarMinFlag = 0
PseudocolorAtts.opacityVarMaxFlag = 0
PseudocolorAtts.pointSize = 0.05
PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
PseudocolorAtts.pointSizeVarEnabled = 0
PseudocolorAtts.pointSizeVar = "default"
PseudocolorAtts.pointSizePixels = 1
PseudocolorAtts.renderSurfaces = 1
PseudocolorAtts.renderWireframe = 0
PseudocolorAtts.renderPoints = 0
PseudocolorAtts.smoothingLevel = 0
PseudocolorAtts.legendFlag = 1
PseudocolorAtts.lightingFlag = 1
SetPlotOptions(PseudocolorAtts)

DrawPlots()

View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (0.524035, 0.708184, 0.473142)
View3DAtts.focus = (6.06386e-11, -1.01151e-12, 9.79488e-06)
View3DAtts.viewUp = (0.839337, -0.335113, -0.428033)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 5.90008e-06
View3DAtts.nearPlane = -1.18002e-05
View3DAtts.farPlane = 1.18002e-05
View3DAtts.imagePan = (0.0451323, 0.118841)
View3DAtts.imageZoom = 0.572468
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (6.06386e-11, -1.01151e-12, 9.79488e-06)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)

AnnotationAtts = AnnotationAttributes()
AnnotationAtts.axes3D.visible = 1
AnnotationAtts.axes3D.autoSetTicks = 1
AnnotationAtts.axes3D.autoSetScaling = 1
AnnotationAtts.axes3D.lineWidth = 0
AnnotationAtts.axes3D.tickLocation = AnnotationAtts.axes3D.Inside  # Inside, Outside, Both
AnnotationAtts.axes3D.axesType = AnnotationAtts.axes3D.OutsideEdges  # ClosestTriad, FurthestTriad, OutsideEdges, StaticTriad, StaticEdges
AnnotationAtts.axes3D.triadFlag = 1
AnnotationAtts.axes3D.bboxFlag = 1
AnnotationAtts.userInfoFlag = 0
AnnotationAtts.databaseInfoFlag = 1
AnnotationAtts.timeInfoFlag = 1
AnnotationAtts.legendInfoFlag = 1
AnnotationAtts.backgroundColor = (0, 0, 0, 255)
AnnotationAtts.foregroundColor = (255, 255, 255, 255)
AnnotationAtts.gradientBackgroundStyle = AnnotationAtts.TopToBottom  # TopToBottom, BottomToTop, LeftToRight, RightToLeft, Radial
AnnotationAtts.gradientColor1 = (128, 128, 128, 255)
AnnotationAtts.gradientColor2 = (0, 0, 0, 255)
AnnotationAtts.backgroundMode = AnnotationAtts.Solid  # Solid, Gradient, Image, ImageSphere
SetAnnotationAttributes(AnnotationAtts)

DrawPlots()

# save the window
SaveWindowAtts = SaveWindowAttributes()
SaveWindowAtts.family = 0
SaveWindowAtts.fileName = '%s-part-u-3d-%04d.png'%(active_var, pu3_im_id)
SaveWindowAtts.format = SaveWindowAtts.PNG  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
SaveWindowAtts.width = 1639
SaveWindowAtts.height = 1227
SaveWindowAtts.resConstraint = SaveWindowAtts.NoConstraint  # NoConstraint, EqualWidthHeight, ScreenProportions
SetSaveWindowAttributes(SaveWindowAtts)
SaveWindow()

DeleteAllPlots()
