
spec = ['Electron-All', 'Carbon-0', 'Proton-0']
#expr = ['e_log_ke', 'c_log_ke', 'p_log_ke']
expr = ['Electron-All/ke', 'Carbon-0/ke', 'Proton-0/ke']
op_out = ['den', 'ke']
nspec = len(spec)
nops = len(op_out)

try:
    dvz_it
except NameError:
    dvz_it = 0
else:
    dvz_it += 1

i = 0
while i < nspec:
    active_spec = spec[i]
    active_expr = expr[i]
    j = 0
    while j < nops:
        AddPlot("Curve", "grid/Ex", 1, 0)

        AddOperator("DataBinning", 0)

        ChangeActivePlotsVar("operators/DataBinning/1D/" + active_spec)

        DataBinningAtts = DataBinningAttributes()

        op = [DataBinningAtts.Count, DataBinningAtts.Average]
        active_op = op[j]

        DataBinningAtts.numDimensions = DataBinningAtts.One  # One, Two, Three
        DataBinningAtts.dim1BinBasedOn = DataBinningAtts.Z  # X, Y, Z, Variable
        DataBinningAtts.dim1Var = "default"
        DataBinningAtts.dim1SpecifyRange = 1
        DataBinningAtts.dim1MinRange = bounds[4]
        DataBinningAtts.dim1MaxRange = bounds[5]
        DataBinningAtts.dim1NumBins = 256
        DataBinningAtts.dim2BinBasedOn = DataBinningAtts.Variable  # X, Y, Z, Variable
        DataBinningAtts.dim2Var = "default"
        DataBinningAtts.dim2SpecifyRange = 0
        DataBinningAtts.dim2MinRange = 0
        DataBinningAtts.dim2MaxRange = 1
        DataBinningAtts.dim2NumBins = 50
        DataBinningAtts.dim3BinBasedOn = DataBinningAtts.Variable  # X, Y, Z, Variable
        DataBinningAtts.dim3Var = "default"
        DataBinningAtts.dim3SpecifyRange = 0
        DataBinningAtts.dim3MinRange = 0
        DataBinningAtts.dim3MaxRange = 1
        DataBinningAtts.dim3NumBins = 50
        DataBinningAtts.outOfBoundsBehavior = DataBinningAtts.Clamp  # Clamp, Discard
        DataBinningAtts.reductionOperator = active_op  # Average, Minimum, Maximum, StandardDeviation, Variance, Sum, Count, RMS, PDF
        DataBinningAtts.varForReduction = active_expr
        DataBinningAtts.emptyVal = 0
        DataBinningAtts.outputType = DataBinningAtts.OutputOnBins  # OutputOnBins, OutputOnInputMesh
        DataBinningAtts.removeEmptyValFromCurve = 0
        SetOperatorOptions(DataBinningAtts, 0)

        DrawPlots()

        # save the data
        ExportDBAtts = ExportDBAttributes()
        ExportDBAtts.allTimes = 0
        ExportDBAtts.db_type = "XYZ"
        ExportDBAtts.db_type_fullname = "XYZ_1.0"
        ExportDBAtts.variables = ("default")
        ExportDBAtts.filename = '%s-%s-vs-z-%04d'%(active_spec, op_out[j], dvz_it)
        ExportDatabase(ExportDBAtts)

        DeleteAllPlots()

        j += 1
    i += 1
