
var = ['Electron-All', 'Carbon-0', 'Proton-0', 'Electron-AllBeam', 'Carbon-Beam', 'Proton-Beam']

try:
    histke_it
except NameError:
    histke_it = 0
else:
    histke_it += 1

for active_var in var:
    AddPlot("Histogram", active_var + "/ke", 1, 0)

    HistogramAtts = HistogramAttributes()
    HistogramAtts.basedOn = HistogramAtts.ManyZonesForSingleVar  # ManyVarsForSingleZone, ManyZonesForSingleVar
    HistogramAtts.histogramType = HistogramAtts.Frequency  # Frequency, Weighted, Variable
    HistogramAtts.weightVariable = "default"
    HistogramAtts.limitsMode = HistogramAtts.OriginalData  # OriginalData, CurrentPlot
    HistogramAtts.minFlag = 1
    HistogramAtts.maxFlag = 1
    HistogramAtts.min = 0
    HistogramAtts.max = 1e10
    HistogramAtts.numBins = 512
    HistogramAtts.domain = 0
    HistogramAtts.zone = 0
    HistogramAtts.useBinWidths = 1
    HistogramAtts.outputType = HistogramAtts.Curve  # Curve, Block
    HistogramAtts.lineStyle = HistogramAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
    HistogramAtts.lineWidth = 2
    HistogramAtts.color = (255, 0, 0, 255)
    HistogramAtts.dataScale = HistogramAtts.Log  # Linear, Log, SquareRoot
    HistogramAtts.binScale = HistogramAtts.Linear  # Linear, Log, SquareRoot
    HistogramAtts.normalizeHistogram = 0
    HistogramAtts.computeAsCDF = 0
    SetPlotOptions(HistogramAtts)

    DrawPlots()

    # save the data
    ExportDBAtts = ExportDBAttributes()
    ExportDBAtts.allTimes = 0
    ExportDBAtts.db_type = "XYZ"
    ExportDBAtts.db_type_fullname = "XYZ_1.0"
    ExportDBAtts.filename = '%s-hist-ke-2d-%04d'%(active_var, histke_it)
    ExportDatabase(ExportDBAtts)

    DeleteAllPlots()
