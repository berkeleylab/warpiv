# Script showing all protons in gray and the beam particles in color

# counter for cycling various script params
try:
    protonbeam_it
except NameError:
    protonbeam_it = 0
else:
    protonbeam_it += 1

proton_vars = ["Proton-0/ke", "Proton-protonbeam/ke"]
for pi, pvar in enumerate(proton_vars):

    plot_all_protons = (pi==0)

    # Add the plot with the protons
    AddPlot("Pseudocolor", pvar, 1, 0)
    PseudocolorAtts = PseudocolorAttributes()
    PseudocolorAtts.scaling = PseudocolorAtts.Linear  # Linear, Log, Skew
    PseudocolorAtts.skewFactor = 1
    PseudocolorAtts.limitsMode = PseudocolorAtts.OriginalData  # OriginalData, CurrentPlot
    PseudocolorAtts.minFlag = 1 if plot_all_protons else 0
    PseudocolorAtts.min = -1
    PseudocolorAtts.maxFlag = 0
    PseudocolorAtts.max = 1
    PseudocolorAtts.centering = PseudocolorAtts.Natural  # Natural, Nodal, Zonal
    PseudocolorAtts.colorTableName = 'xray' if plot_all_protons else 'hot'
    PseudocolorAtts.invertColorTable = 0
    PseudocolorAtts.opacityType = PseudocolorAtts.Constant  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
    PseudocolorAtts.opacityVariable = ""
    PseudocolorAtts.opacity = 1.0
    PseudocolorAtts.opacityVarMin = 0
    PseudocolorAtts.opacityVarMax = 1
    PseudocolorAtts.opacityVarMinFlag = 0
    PseudocolorAtts.opacityVarMaxFlag = 0
    PseudocolorAtts.pointSize = 0.05
    PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
    PseudocolorAtts.pointSizeVarEnabled = 0
    PseudocolorAtts.pointSizeVar = "default"
    PseudocolorAtts.pointSizePixels = 2
    PseudocolorAtts.renderSurfaces = 1
    PseudocolorAtts.renderWireframe = 0
    PseudocolorAtts.renderPoints = 0
    PseudocolorAtts.smoothingLevel = 0
    PseudocolorAtts.legendFlag = 1
    PseudocolorAtts.lightingFlag = 1
    SetPlotOptions(PseudocolorAtts)

    # Rotate the plot using Transform to swap the x/z axis in the 2D plot
    AddOperator("Transform", 0)
    TransformAtts = TransformAttributes()
    TransformAtts.doRotate = 1
    TransformAtts.rotateOrigin = (0, 0, 0)
    TransformAtts.rotateAxis = (0, 1, 0)
    TransformAtts.rotateAmount = 90
    TransformAtts.rotateType = TransformAtts.Deg  # Deg, Rad
    TransformAtts.doScale = 1
    TransformAtts.scaleOrigin = (0, 0, 0)
    TransformAtts.scaleX = 1
    TransformAtts.scaleY = 1
    TransformAtts.scaleZ = -1
    SetOperatorOptions(TransformAtts, 0)
    # Project the data to 2D
    AddOperator("Project", 0)
    ProjectAtts = ProjectAttributes()
    ProjectAtts.projectionType = ProjectAtts.XZCartesian  # ZYCartesian, XZCartesian, XYCartesian, XRCylindrical, YRCylindrical, ZRCylindrical
    SetOperatorOptions(ProjectAtts, 0)

View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.0, 1.2e-05, -3.6e-06, 3.6e-06)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)

DrawPlots()
saveWin('protons_vs_beam_2d_-%04d.png'%(protonbeam_it))

# Query the beam plot for the required data
#SetActivePlots(1)
#recordStats("Proton-protonbeam/ke", actual=1)
#SetActivePlots(0)
#recordStats("Proton-protonbeam/ke", actual=1)

DeleteAllPlots()
