
var = ['Electron-All', 'Carbon-0', 'Proton-0']
lut = ['electron_den', 'carbon_den', 'proton_den']
clip = [1, 1, 1]

try:
    pd3a_it
except NameError:
    pd3a_it = 0
else:
    pd3a_it += 1


# this ensures a consistent axes
AddPlot("Mesh", "grid", 1, 0)
DrawPlots()
HideActivePlots()

i = 0
while i < 3:
    active_var = var[i]

    # density computed by data binning doesn't
    # yet exist.
    AddPlot("Pseudocolor", active_var + "/e_magnitude", 1, 0)

    AddOperator("DataBinning",0)
    DataBinningAtts = DataBinningAttributes()
    DataBinningAtts.numDimensions = DataBinningAtts.Three  # One, Two, Three
    DataBinningAtts.dim1BinBasedOn = DataBinningAtts.X  # X, Y, Z, Variable
    DataBinningAtts.dim1NumBins = int(nx)
    DataBinningAtts.dim2BinBasedOn = DataBinningAtts.Y  # X, Y, Z, Variable
    DataBinningAtts.dim2NumBins = int(ny)
    DataBinningAtts.dim3BinBasedOn = DataBinningAtts.Z  # X, Y, Z, Variable
    DataBinningAtts.dim3NumBins = int(nz)
    DataBinningAtts.reductionOperator = DataBinningAtts.Count  # Average, Minimum, Maximum, StandardDeviation, Variance, Sum, Count, RMS, PDF
    DataBinningAtts.emptyVal = 0
    DataBinningAtts.outputType = DataBinningAtts.OutputOnInputMesh  # OutputOnBins, OutputOnInputMesh
    SetOperatorOptions(DataBinningAtts, 0)

    # fix pcolor variable
    ChangeActivePlotsVar("operators/DataBinning/3D/" + active_var)

    PseudocolorAtts = PseudocolorAttributes()
    PseudocolorAtts.limitsMode = PseudocolorAtts.OriginalData  # OriginalData, CurrentPlot
    PseudocolorAtts.minFlag = 1
    PseudocolorAtts.min = 0
    PseudocolorAtts.maxFlag = 0
    PseudocolorAtts.max = 1
    PseudocolorAtts.colorTableName = lut[i]
    PseudocolorAtts.invertColorTable = 0
    PseudocolorAtts.opacityType = PseudocolorAtts.Ramp  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
    PseudocolorAtts.opacityVariable = ""
    PseudocolorAtts.opacity = 1.0
    PseudocolorAtts.pointSize = 0.05
    PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
    PseudocolorAtts.pointSizePixels = 1
    SetPlotOptions(PseudocolorAtts)

    if clip[i]:
        AddOperator("Clip", 0)
        ClipAtts = ClipAttributes()
        ClipAtts.quality = ClipAtts.Fast  # Fast, Accurate
        ClipAtts.funcType = ClipAtts.Plane  # Plane, Sphere
        ClipAtts.plane1Status = 1
        ClipAtts.plane2Status = 1
        ClipAtts.plane3Status = 0
        ClipAtts.plane1Origin = (0, 0, 0)
        ClipAtts.plane2Origin = (0, 0, 0)
        ClipAtts.plane3Origin = (0, 0, 0)
        ClipAtts.plane1Normal = (1, 0, 0)
        ClipAtts.plane2Normal = (0, 1, 0)
        ClipAtts.plane3Normal = (0, 0, 1)
        ClipAtts.planeInverse = 0
        ClipAtts.planeToolControlledClipPlane = ClipAtts.Plane1  # None, Plane1, Plane2, Plane3
        ClipAtts.center = (0, 0, 0)
        ClipAtts.radius = 1
        ClipAtts.sphereInverse = 0
        SetOperatorOptions(ClipAtts, 0)

    i += 1


DrawPlots()
setCam()
setAtts()
DrawPlots()
saveWin('all-part-den-3d-%04d.png'%(pd3a_it))

DeleteAllPlots()
