import os
import sys
import argparse
import numpy
import warpoptions
warpoptions.lskipoptions = 1
import warp
from WarpIVSimulation import WarpIVSimulation
from WarpIVFilteredSpecies import WarpIVSpeciesFilters
from WarpIVMergedSpecies import WarpIVMergedSpecies
from WarpIVUtil import pStatus, pError

#----------------------------------------------------------------------------
def NewWarpIVSimulation(args=[]):
    """
    factory that creates an obect implementing WarpIVSimulation
    class interface
    """
    return IonSimulation(args)

#----------------------------------------------------------------------------
def GetCommandLineHelp():
    """
    return the command line options without creating the object.
    """
    return IonSimulation.GetCommandLineHelp()

#----------------------------------------------------------------------------
class IonSimulation(WarpIVSimulation):
    """
    Implementation of the WarpIVSimulation class for ion accelerator
    """
    def __init__(self,args=[]):
        # call base class constructor
        WarpIVSimulation.__init__(self,args)

        # for two phases of vis output frequency
        # adopt superclass settings as the default
        # so these don't have to be set by user.
        self._InitStep = self._Step
        self._InitStop = self._Stop
        self._InitPlot = self._Plot
        self._NDim = 2
        self._DxFact = 16
        self._XFact = 0.5
        self._ZFact = 1.25
        self._NPCFact = 1.0
        self._DimScale = 2.0
        self._DataBin = False
        self._BinImages = False
        self._Restart = ''
        self._InterpreterInitialized = False

        # parse command line args
        ap = argparse.ArgumentParser(usage=argparse.SUPPRESS,prog='IonSimulation',add_help=False)
        ap.add_argument('--init-step',type=int,default=self._InitStep)
        ap.add_argument('--init-stop',type=int,default=self._InitStop)
        ap.add_argument('--init-plot',type=int,default=None)
        ap.add_argument('--ndim',type=int,default=self._NDim)
        ap.add_argument('--dxfact',type=int,default=self._DxFact)
        ap.add_argument('--xfact',type=float,default=self._XFact)
        ap.add_argument('--zfact',type=float,default=self._ZFact)
        ap.add_argument('--npcfact',type=float,default=self._NPCFact)
        ap.add_argument('--dim-scale',type=float,default=self._DimScale)
        ap.add_argument('--data-bin',action='store_true')
        ap.add_argument('--bin-images', action='store_true')
        ap.add_argument('--restart',type=str,default=self._Restart)
        opts = vars(ap.parse_known_args(args)[0])
        opts = vars(ap.parse_known_args(args)[0])
        self._InitStep = opts['init_step']
        self._InitStop = opts['init_stop']
        self._InitPlot = self._InitStep if opts['init_plot'] is None else opts['init_plot']
        self._NDim = opts['ndim']
        self._DxFact = opts['dxfact']
        self._XFact = opts['xfact']
        self._ZFact = opts['zfact']
        self._NPCFact = opts['npcfact']
        self._DimScale = opts['dim_scale']
        self._DataBin = opts['data_bin'] if 'data_bin' in opts else False
        self._BinImages = opts['bin_images'] if 'bin_images' in opts else False
        self._Restart = opts['restart']

        # setup vis scripts here
        if self._NDim == 3:
            #self.AddScript('pden', 'part-den-3d.py')
            #self.AddScript('pdena', 'part-den-all-3d.py')
            #self.AddScript('pke', 'part-ke-3d.py')
            #self.AddScript('b_pkea', 'part-ke-all-3d.py')
            #self.AddScript('vol', 'vol-3d.py')
            #self.AddScript('iso', 'iso-3d.py')
            #self.AddScript('b_isoa', 'iso-all-3d.py')
            pass
        else:
            #self.AddScript('pden', 'part-den-2d.py')
            #self.AddScript('pke', 'part-ke-2d.py')
            #self.AddScript('pdena', 'part-den-all-2d.py')
            #self.AddScript('pkea', 'part-ke-all-2d.py')
            self.AddScript('b_pcey', 'pc-ey-2d.py')
            #self.AddScript('pckea', 'pc-ke-all-2d.py')
            #self.AddScript('pcdena', 'pc-den-all-2d.py')
            #self.AddScript('b_beam', 'protons_vs_beam.py')

        self.AddScript('a_cliCommon', 'cliCommon.py')
        self.AddScript('a_simCommon', 'simCommon.py', scriptType='sim')
        self.AddScript('b_pcxz', 'pc-xz-2d.py')
        self.AddScript('b_pcxy', 'pc-xy-2d.py')
        self.AddScript('b_pcyz', 'pc-yz-2d.py')
        self.AddScript('b_hiske', 'hist-ke.py')
        self.AddScript('b_vsz', 'den-ke-vs-z.py')
        self.AddScript('b_queries', 'queries.py', scriptType='sim')
        self.AddScript('b_beamke', 'hist-ke-proton-beam.py')

        setSimDeltas(self._DxFact)
        bounds, nx = getSimBounds(self._XFact, self._ZFact)
        gvars = 'dxfact=%d\n'%(self._DxFact)
        gvars += 'bounds=%s\n'%(bounds)
        gvars += 'nx=%s\n'%(nx[0])
        gvars += 'ny=%s\n'%(nx[1])
        gvars += 'nz=%s\n'%(nx[2])
        gvars += 'dim_scale=%s\n'%(self._DimScale)
        gvars += 'dataBin=%d\n'%(self._DataBin)
        gvars += 'binImages=%d\n'%(self._BinImages)
        self.AddScript('a_globals', sourceString=gvars)

        return

    #------------------------------------------------------------------------
    def Initialize(self):
        """
        Setup IC and start the simulation.
        """
        pStatus('Initializing...')

        # call scientist provided script to intialize warp
        initIonSim(self._NDim, self._DxFact, self._XFact, self._ZFact, \
            self._NPCFact, self._Restart)

        # call default implementation
        WarpIVSimulation.Initialize(self)

        pStatus('Initialization complete!')
        return

    #------------------------------------------------------------------------
    def GetActiveScripts(self):
        """
        return the list of scripts to render at the current step
        """
        scripts = []
        if (warp.warp.top.it > 0) \
            and (self._Plot > 0) \
            and (warp.warp.top.it%self._Plot == 0):

            # globals, color tables, shared functions etc
            if not self._InterpreterInitialized:
                scripts += ['a_globals', 'a_simCommon', 'a_cliCommon']
                self._InterpreterInitialized = True

            # used in both 2d and 3d runs
            scripts += ['b_queries', 'b_pcxz', 'b_hiske', 'b_vsz']

            # only 3d
            if self._NDim == 3:
                scripts += ['b_isoa', 'b_pcxy', 'b_pcyz']
            # only 2d
            else:
                scripts += ['b_pcey']

        return scripts

    #------------------------------------------------------------------------
    def Advance(self):
        """
        Specialization for two phases of plotting, an inital presumably
        low frequency phase where we monitor sim nstartup, and a final
        presumably high frequency phase to capture results.
        """
        if warp.warp.top.it < self._InitStop:
            # initial
            warp.step(self._InitStep)
        else:
            # final
            return super(IonSimulation,self).Advance()

# ---------------------------------------------------------------------------
# helper functions
# ---------------------------------------------------------------------------

# global varss used by the init script
laser_amplitude = None
laser_phase = None
laser_profile = None
laser_total_duration = None
laser_waist = None
k0 = None
w0 = None
ZR = None
Eamp = None
carbon_layer_start     = 6
carbon_layer_width     = 9
carbon_layer_thickness = 0.075
carbon_layer_e_density = 400.
lambda_laser           = 0.8e-6                      # wavelength
dt=0.0025
dx=dy=dz=0.005

def setSimDeltas(dxfact):
    global dx, dy, dz, dt
    dx*=dxfact
    dy=dz=dx
    dt*=dxfact

def getSimBounds(xfact, zfact):
    global carbon_layer_start, carbon_layer_width, \
        carbon_layer_thickness, carbon_layer_e_density, \
        lambda_laser, dx, dy, dz, dt

    bounds = []
    bounds.append(-xfact*carbon_layer_width*lambda_laser)
    bounds.append(-bounds[0])
    bounds.append(-xfact*carbon_layer_width*lambda_laser)
    bounds.append(-bounds[2])
    bounds.append(0.0)
    bounds.append(zfact*carbon_layer_start*2*lambda_laser)

    nx = []
    nx.append(warp.nint((bounds[1]-bounds[0])/(dx*lambda_laser)))
    nx.append(warp.nint((bounds[3]-bounds[2])/(dy*lambda_laser)))
    nx.append(warp.nint((bounds[5]-bounds[4])/(dz*lambda_laser)))

    return bounds, nx

def initIonSim(ndim, dxf, xfact, zfact, npcfact, dumpfn):
    global laser_amplitude, laser_phase, laser_profile, \
        laser_total_duration, laser_waist, k0, w0, ZR, Eamp, \
        carbon_layer_start, carbon_layer_width, \
        carbon_layer_thickness, carbon_layer_e_density, \
        lambda_laser, dx, dy, dz, dt

    pStatus('dim=%d, xfact=%f, zfact=%f, dxfact=%f, npcfact=%f'%( \
        ndim, xfact, zfact, dxf, npcfact))

    # --- flags turning off unnecessary diagnostics (ignore for now)
    warp.top.ifzmmnt = 0
    warp.top.itmomnts = 0
    warp.top.itplps = 0
    warp.top.itplfreq = 0
    warp.top.zzmomnts = 0
    warp.top.zzplps = 0
    warp.top.zzplfreq = 0
    warp.top.nhist = warp.top.nt
    warp.top.iflabwn = 0
    warp.w3d.lrhodia3d = warp.false
    warp.w3d.lgetese3d = warp.false
    warp.w3d.lgtlchg3d = warp.false

    # ----------
    # Parameters
    # ----------

    dfact = 1
    dxfact = dxf
    dtfact = dxf
    N_step = 20000/dtfact

    # for debugging VisIt set this to 1
    nppc_scale = npcfact

    #Two-layer foil:
    #Carbon layer
    #carbon_layer_start     = 6
    #carbon_layer_width     = 9
    #carbon_layer_thickness = 0.075
    #carbon_layer_e_density = 400.
    nppcell_carbon         = 25*nppc_scale
    #Hydrogen layer
    hydrogen_layer_width     = 9
    hydrogen_layer_thickness = 0.05
    hydrogen_layer_e_density = 21.
    nppcell_hydrogen         = 16*nppc_scale

    #Laser at the left border:
    a0             = 100
    laser_duration = 10
    laser_width    = 4/2 #(=2w_0)
    #S-pol
    #Transverse profile - Gaussian
    #Longitudinal profile Super-Gaussian 12
    #focused at x=6 from the left border

    #Mesh:
    #dt=0.0025
    #dx=dy=dz=0.005

    # --- scaling
    carbon_layer_e_density/=dfact
    hydrogen_layer_e_density/=dfact
    #dx*=dxfact
    #dy=dz=dx
    #dt*=dtfact

    #-------------------------------------------------------------------------------
    # main parameters
    #-------------------------------------------------------------------------------
    dim = '3d' if ndim == 3 else '2d' # 1D/2D/3D calculation
    dpi=100                     # graphics resolution
    l_test             = 0      # Will open output window on screen
                                # and stop before entering main loop.
    l_gist             = 1      # Turns gist plotting on/off
    l_restart          = warp.false  # To restart simulation from an old run (works?)
    restart_dump       = ""     # dump file to restart from (works?)
    l_moving_window    = 1      # on/off (Galilean) moving window
    l_plasma           = 1      # on/off plasma
    l_usesavedist      = 0      # if on, uses dump of beam particles distribution
    l_smooth           = 1      # on/off smoothing of current density
    l_laser            = 1      # on/off laser
    l_pdump            = 0      # on/off regular dump of beam data
    stencil            = 1      # 0 = Yee; 1 = Yee-enlarged (Karkkainen) on EF,B; 2 = Yee-enlarged (Karkkainen) on E,F
                                # use 0 or 1; 2 does not verify Gauss Law
    if dim=="1d":stencil=0
    dtcoef             = dt/dx  # coefficient to multiply default time step that is set at the EM solver CFL
    warp.top.depos_order    = 3      # particles deposition order (1=linear, 2=quadratic, 3=cubic)
    warp.top.efetch         = 4      # field gather type (1=from nodes "momentum conserving"; 4=from Yee mesh "energy conserving")

    warp.top.runid          = "ion_acceleration"  # run name
    warp.top.pline1         = "basic lpa"         # comment line on plots
    warp.top.runmaker       = "J.-L. Vay,"        # run makers
    warp.top.lrelativ       = warp.true           # on/off relativity (for particles push)
    warp.top.pgroup.lebcancel_pusher=warp.true    # flag for particle pusher (0=Boris pusher; 1=Vay PoP 08 pusher)
    l_verbose          = 0                        # verbosity level (0=off; 1=on)

    #-------------------------------------------------------------------------------
    # diagnostics parameters + a few other settings
    #-------------------------------------------------------------------------------
    live_plot_freq     = 1000   # frequency (in time steps) of live plots (off is l_test is off)

    fielddiag_period   = 500/dtfact
    partdiag_period    = 500/dtfact

    #-------------------------------------------------------------------------------
    # laser parameters
    #-------------------------------------------------------------------------------
    # --- in lab frame
    #lambda_laser       = 0.8e-6                      # wavelength
    laser_width       *= lambda_laser
    laser_waist        = laser_width/2
    laser_radius       = laser_waist/2.354820        # FWHM -> radius
    laser_duration     = laser_duration*lambda_laser/warp.clight # laser duration
    laser_polangle     = 0#warp.pi/2                 # polarization (0=aligned with x; warp.pi/2=aligned with y)
    k0                 = 2.*warp.pi/lambda_laser
    w0                 = k0*warp.clight
    ZR                 = 0.5*k0*(laser_waist**2)     # Rayleigh length
    Eamp               = a0*w0*warp.emass*warp.clight/warp.echarge
    Bamp               = Eamp/warp.clight
    if l_laser==0:Eamp=Bamp=0.

    #-------------------------------------------------------------------------------
    # plasma layers
    #-------------------------------------------------------------------------------
    dfact             = 1.                                  # coefficient factor for plasma density (for scaled simulations)
    densc             = warp.emass*warp.eps0*w0**2/warp.echarge**2    # critical density

    #-------------------------------------------------------------------------------
    # Carbon plasma
    #-------------------------------------------------------------------------------
    dens0_C           = dfact*carbon_layer_e_density*densc    # plasma density
    wp_C              = numpy.sqrt(dens0_C*warp.echarge**2/(warp.eps0*warp.emass)) # plasma frequency
    kp_C              = wp_C/warp.clight                      # plasma wavenumber
    lambda_plasma_C   = 2.*warp.pi/kp_C                       # plasma wavelength

    #-------------------------------------------------------------------------------
    # Carbon plasma
    #-------------------------------------------------------------------------------
    dens0_H           = dfact*hydrogen_layer_e_density*densc  # plasma density
    wp_H              = numpy.sqrt(dens0_H*warp.echarge**2/(warp.eps0*warp.emass)) # plasma frequency
    kp_H              = wp_H/warp.clight                      # plasma wavenumber
    lambda_plasma_H   = 2.*warp.pi/kp_H                       # plasma wavelength

    #-------------------------------------------------------------------------------
    # print some plasma parameters to the screen
    #-------------------------------------------------------------------------------
    print("the laser spot size is: ")
    print laser_waist
    print("the Rayleigh length is: ")
    print ZR
    print("the laser wavelength is: ")
    print lambda_laser
    print("the Carbon plasma wavelength is: ")
    print lambda_plasma_C
    print("the Hydrogen plasma wavelength is: ")
    print lambda_plasma_H

    #-------------------------------------------------------------------------------
    # number of plasma macro-particles/cell
    #-------------------------------------------------------------------------------
    nppcellx_C = 5*nppc_scale
    nppcelly_C = 5*nppc_scale
    nppcellz_C = 5*nppc_scale

    nppcellx_H = 4*nppc_scale
    nppcelly_H = 4*nppc_scale
    nppcellz_H = 4*nppc_scale

    if dim=="2d":
      nppcelly_C = nppcelly_H = 1
    if dim=="1d":
      nppcellx_C = nppcellx_H = 1
      nppcelly_C = nppcelly_H = 1

    #-------------------------------------------------------------------------------
    # grid dimensions, nb cells and BC
    #-------------------------------------------------------------------------------
    warp.w3d.zmmax = zfact*carbon_layer_start*2*lambda_laser
    warp.w3d.zmmin = 0.
    warp.w3d.xmmin = -xfact*carbon_layer_width*lambda_laser
    warp.w3d.xmmax = -warp.w3d.xmmin
    warp.w3d.ymmin = -xfact*carbon_layer_width*lambda_laser
    warp.w3d.ymmax = -warp.w3d.ymmin

    warp.w3d.nx = warp.nint((warp.w3d.xmmax-warp.w3d.xmmin)/(dx*lambda_laser))
    warp.w3d.ny = warp.nint((warp.w3d.ymmax-warp.w3d.ymmin)/(dy*lambda_laser))
    warp.w3d.nz = warp.nint((warp.w3d.zmmax-warp.w3d.zmmin)/(dz*lambda_laser))

    pStatus('grid dims are %d %d %d'%(warp.w3d.nx, warp.w3d.ny, warp.w3d.nz))
    pStatus('grid bounds are %g %g %g %g %g %g'%( \
        warp.w3d.xmmin, warp.w3d.xmmax, \
        warp.w3d.xmmin, warp.w3d.xmmax, \
        warp.w3d.zmmin, warp.w3d.zmmax))

    # note: these y dims are huge compared to x and z but warp needs
    # that. we'll have to scale this axis in visit.
    if dim in ["1d"]:
        warp.w3d.nx = 2
        warp.w3d.xmmin = -float(warp.w3d.nx)/2
        warp.w3d.xmmax = float(warp.w3d.nx)/2
    if dim in ["1d","2d"]:
        warp.w3d.ny = 2
        warp.w3d.ymmin = -float(warp.w3d.ny)/2
        warp.w3d.ymmax = float(warp.w3d.ny)/2

    warp.w3d.dx = (warp.w3d.xmmax-warp.w3d.xmmin)/warp.w3d.nx
    warp.w3d.dy = (warp.w3d.ymmax-warp.w3d.ymmin)/warp.w3d.ny
    warp.w3d.dz = (warp.w3d.zmmax-warp.w3d.zmmin)/warp.w3d.nz

    # --- sets field boundary conditions
    # --- longitudinal
    warp.w3d.bound0  = warp.w3d.boundnz = warp.openbc
    # --- transverse
    warp.w3d.boundxy = warp.periodic

    # --- sets particles boundary conditions
    # --- longitudinal
    warp.top.pbound0  = warp.absorb
    warp.top.pboundnz = warp.absorb
    # --- transverse
    warp.top.pboundxy = warp.periodic

    #-------------------------------------------------------------------------------
    # set particles weights
    #-------------------------------------------------------------------------------
    weight_C   = dens0_C*warp.w3d.dx*warp.w3d.dy*warp.w3d.dz/(nppcellx_C*nppcelly_C*nppcellz_C)
    weight_H   = dens0_H*warp.w3d.dx*warp.w3d.dy*warp.w3d.dz/(nppcellx_C*nppcelly_C*nppcellz_C)
    warp.top.wpid = warp.nextpid() # Activate variable weights in the method addpart

    # --- create plasma species
    elec_C = warp.Species(type=warp.Electron,weight=weight_C)
    elec_H = warp.Species(type=warp.Electron,weight=weight_H)
    ions_C = warp.Species(type=warp.Carbon,weight=weight_C/6,charge_state=6.)
    ions_H = warp.Species(type=warp.Proton,weight=weight_H)

    warp.top.depos_order[...] = warp.top.depos_order[0,0] # sets deposition order of all species = those of species 0
    warp.top.efetch[...] = warp.top.efetch[0] # same for field gathering
    if dim in ["1d","2d"]:
      warp.top.depos_order[1,:]=1
    if dim=="1d":
      warp.top.depos_order[0,:]=1

    #-------------------------------------------------------------------------------
    # set smoothing of current density
    #-------------------------------------------------------------------------------
    if l_smooth:
      # --- 1 time nilinear (0.25,0.5,0.25) + 1 time relocalization (-1, 3/2,-1.)
      npass_smooth = [[ 1 , 1 ],[ 0 , 0 ],[ 1 , 1 ]]
      alpha_smooth = [[ 0.5, 3./2],[ 0.5, 3.],[0.5, 3./2]]
      stride_smooth = [[ 1 , 1 ],[ 1 , 1 ],[ 1 , 1 ]]
      if dim=='1d':
        for i in range(len(npass_smooth[0])):
          npass_smooth[0][i]=0
      if dim in ['1d','2d']:
        for i in range(len(npass_smooth[0])):
          npass_smooth[1][i]=0
    else:
      npass_smooth = [[ 0 ],[ 0 ],[ 0 ]]
      alpha_smooth = [[ 1.],[ 1.],[ 1.]]
      stride_smooth = [[ 1 ],[ 1 ],[ 1 ]]

    #-------------------------------------------------------------------------------
    # initializes WARP
    #-------------------------------------------------------------------------------
    warp.top.fstype = -1 # sets field solver to None (desactivates electrostatic solver)
    warp.package('w3d');
    warp.generate()

    #-------------------------------------------------------------------------------
    # set a few shortcuts
    #-------------------------------------------------------------------------------
    pg = warp.top.pgroup

    if l_plasma:

        zmin = carbon_layer_start*lambda_laser
        zmax = zmin+carbon_layer_thickness*lambda_laser
        xmin = warp.w3d.xmmin
        xmax = warp.w3d.xmmax
        if dim=='3d':
            ymin=xmin
            ymax=xmax
            np = warp.w3d.nx*warp.w3d.ny*warp.nint((zmax-zmin)/warp.w3d.dz)*nppcellx_C*nppcelly_C*nppcellz_C
        else:
            ymin=ymax=0.
            np = warp.w3d.nx*warp.nint((zmax-zmin)/warp.w3d.dz)*nppcellx_C*nppcelly_C*nppcellz_C

        elec_C.add_uniform_box(np,xmin,xmax,ymin,ymax,zmin,zmax,
                           vthx=0.,vthy=0.,vthz=0.,lallindomain=1,
                           spacing='uniform')

        ions_C.add_uniform_box(np,xmin,xmax,ymin,ymax,zmin,zmax,
                           vthx=0.,vthy=0.,vthz=0.,lallindomain=1,
                           spacing='uniform')

        zmin=zmax+0.
        zmax+=hydrogen_layer_thickness*lambda_laser
        if dim=='3d':
            ymin=xmin
            ymax=xmax
            np = warp.w3d.nx*warp.w3d.ny*warp.nint((zmax-zmin)/warp.w3d.dz)*nppcellx_H*nppcelly_H*nppcellz_H
        else:
            ymin=ymax=0.
            np = warp.w3d.nx*warp.nint((zmax-zmin)/warp.w3d.dz)*nppcellx_H*nppcelly_H*nppcellz_H

        elec_H.add_uniform_box(np,xmin,xmax,ymin,ymax,zmin,zmax,
                           vthx=0.,vthy=0.,vthz=0.,lallindomain=1,
                           spacing='uniform')

        ions_H.add_uniform_box(np,xmin,xmax,ymin,ymax,zmin,zmax,
                           vthx=0.,vthy=0.,vthz=0.,lallindomain=1,
                           spacing='uniform')

    laser_total_duration=1.25*laser_duration

    #-------------------------------------------------------------------------------
    # set laser pulse shape
    #-------------------------------------------------------------------------------
    def laser_amplitude(time):
     global laser_total_duration,Eamp
     #fx=Exp[-((x-t)*2/xblen)]^12
     #xblen=10 - duration of the pulse
     return Eamp*numpy.exp(-(2*(time-0.5*laser_total_duration)/laser_duration)**12)

    def laser_profile(x,y,z):
      global laser_waist, ZR
      #fy=Exp[-(y*2/yblen)^2]
      #yblen=4
      r2 = x**2 + y**2
      W0 = laser_waist
      Wz = W0*numpy.sqrt(1.+(z/ZR)**2) # Beam radius varies along propagation direction
      return (W0/Wz)*numpy.exp(-r2/Wz**2)

    #-------------------------------------------------------------------------------
    # set laser amplitude by combining the pulse shape, laser profile, and laser phase
    #-------------------------------------------------------------------------------

    if l_laser:
      laser_source_z=10*warp.w3d.dz
    else:
      laser_func=laser_source_z=Eamp=None

    #-------------------------------------------------------------------------------
    def laser_func(x,y,t):
      global laser_amplitude,laser_phase,laser_profile,k0,w0,ZR
      z0 = (carbon_layer_start*lambda_laser-laser_source_z)
      Rz = z0*(1.+(ZR/z0)**2)
      E = laser_amplitude(t)*laser_profile(x,y,z0)
      r = numpy.sqrt(x*x+y*y)
      angle = w0*t+k0*z0-numpy.arctan(z0/ZR)+k0*r**2/(2.*Rz)
      #return [E*cos(angle),E*numpy.sin(angle)]   # for circularly polarized laser
      return [0,E*numpy.sin(angle)]               # for linearly polarized laser
    #  return [E*numpy.sin(angle),0]               # for linearly polarized laser

    #-------------------------------------------------------------------------------
    # initializes main field solver block
    #-------------------------------------------------------------------------------
    em = warp.EM3D(  laser_func=laser_func,
                     laser_source_z=laser_source_z,
                     laser_polangle=laser_polangle,
                     laser_emax=Eamp,
                     stencil=stencil,
                     npass_smooth=npass_smooth,
                     alpha_smooth=alpha_smooth,
                     stride_smooth=stride_smooth,
                     l_2dxz=dim=="2d",
                     l_1dz=dim=="1d",
                     dtcoef=dtcoef,
                     l_getrho=1,
                     l_verbose=l_verbose)

    #-------------------------------------------------------------------------------
    # restarts from dump file
    #-------------------------------------------------------------------------------
    if dumpfn:
        pStatus('restarting the simulation from %s'%(dumpfn))
        warp.restart(dumpfn)
        pStatus('restart loaded!')

    #-------------------------------------------------------------------------------
    # register solver
    #-------------------------------------------------------------------------------
    print 'register solver'
    warp.registersolver(em)
    print 'done'

    solver = warp.getregisteredsolver()
    decomp = solver.fsdecomp
    pStatus('grid is decomposed in %d x %d x %d blocks'%( \
        decomp.nxprocs, decomp.nyprocs, decomp.nzprocs))

    #-------------------------------------------------------------------------------
    # add derived species

    # merge E0 and E1 int EAll
    warp.listofallspecies.append( \
        WarpIVMergedSpecies(warp.listofallspecies[0:2], 'All'))

    # add beam filters
    for spec in warp.listofallspecies[2:5]:
        warp.listofallspecies.append( \
            WarpIVSpeciesFilters.CreateFilteredSpecies( \
                spec, 'ConeFilter', 'Beam', filterAngle=8))

    #-------------------------------------------------------------------------------
    # print the list of all available species
    i=0
    for s in warp.listofallspecies:
        print 'species %d %s %s'%(i,s.type.name,str(s.name))
        i += 1

    if (len(warp.listofallspecies) < 1):
        raise RuntimeError('no particle species!')
