
def copylut(lut):
    newlut = ColorControlPointList()
    ncp = ColorControlPoint()
    n = lut.GetNumControlPoints()
    i=0
    while i < n:
        cp = lut.GetControlPoints(i)
        newlut.AddControlPoints(cp)
        i += 1
    return newlut

def mx(a,b=85):
  return a if a > b else b

def mn(a,b=10):
  return a if a < b else b

def scaledlut(r, g, b, a, crgb, ca):
    lut = copylut(GetColorTable('gray'))
    n = lut.GetNumControlPoints()
    d = 1.0/(n-1.0)
    i = 0
    while i < n:
        cc = lut.GetControlPoints(i).colors
        lut.GetControlPoints(i).colors = (mx(cc[0],crgb)/255.0*r, \
            mx(cc[1],crgb)/255.0*g, mx(cc[2],crgb)/255.0*b, \
            mx(i*d*255,ca)*a/255.0)
        i += 1
    return lut

def appendluts(names):
    newlut = ColorControlPointList()
    numLuts = len(names)
    delta = 1.0/numLuts
    i = 0
    while i < numLuts:
        lut = GetColorTable(names[i])
        npts = lut.GetNumControlPoints()
        j = 0
        while j < npts:
            cp = lut.GetControlPoints(j)
            cp.position = cp.position*delta + i*delta
            newlut.AddControlPoints(cp)
            j += 1
        i += 1
    return newlut

def recordMinMax(varName, actual=1):
    SetQueryFloatFormat("%g")
    SetQueryOutputToValue()
    varMin = Query("Min", actual)
    varMax = Query("Max", actual)
    pStatus('range %s = [%g, %g]'%(varName, varMin, varMax))
    return [varMin, varMax]

def recordStats(varName, actual=1):
    SetQueryFloatFormat("%g")
    SetQueryOutputToValue()
    varStats = Query("Sample Statistics", actual)
    varNumZones = Query("NumZones", actual)
    varMin = Query("Min", actual)
    varMax = Query("Max", actual)
    # stats = min, max, avg, sdev, var, skew, kurt, count
    stats = [varMin, varMax, varStats[0], varStats[1], varStats[2], \
         varStats[3], varStats[4], varNumZones]
    pStatus('stats %s = [%g, %g, %g, %g, %g, %g, %g, %g]'%(varName, \
        varMin, varMax, varStats[0], varStats[1], varStats[2], \
        varStats[3], varStats[4], varNumZones))
    return stats

def clipQuadrant1():
    AddOperator("Box", 0)
    BoxAtts = BoxAttributes()
    BoxAtts.amount = BoxAtts.Some  # Some, All
    BoxAtts.minx = 0
    BoxAtts.maxx = 1
    BoxAtts.miny = 0
    BoxAtts.maxy = 1
    BoxAtts.minz = 0
    BoxAtts.maxz = 1
    BoxAtts.inverse = 1
    SetOperatorOptions(BoxAtts, 0)

def setCam():
    View3DAtts = View3DAttributes()
    View3DAtts.viewNormal = (0.317417, 0.830366, 0.457973)
    View3DAtts.focus = (-4.17642e-12, 4.04705e-12, 8.57302e-06)
    View3DAtts.viewUp = (0.94457, -0.234145, -0.230139)
    View3DAtts.viewAngle = 30
    View3DAtts.parallelScale = 9.00438e-06
    View3DAtts.nearPlane = -1.80087e-05
    View3DAtts.farPlane = 1.80087e-05
    View3DAtts.imagePan = (0.0358906, 0.0312601)
    View3DAtts.imageZoom = 1.44562
    View3DAtts.perspective = 1
    View3DAtts.eyeAngle = 2
    View3DAtts.centerOfRotationSet = 0
    View3DAtts.centerOfRotation = (-4.17643e-12, 4.04705e-12, 8.57301e-06)
    View3DAtts.axis3DScaleFlag = 0
    View3DAtts.axis3DScales = (1, 1, 1)
    View3DAtts.shear = (0, 0, 1)
    View3DAtts.windowValid = 1
    SetView3D(View3DAtts)

def setAtts(bg='k'):
    axvis = 1
    AnnotationAtts = AnnotationAttributes()
    AnnotationAtts.axes3D.visible = 1
    AnnotationAtts.axes3D.autoSetTicks = 1
    AnnotationAtts.axes3D.autoSetScaling = 1
    AnnotationAtts.axes3D.lineWidth = 1
    AnnotationAtts.axes3D.tickLocation = AnnotationAtts.axes3D.Inside  # Inside, Outside, Both
    AnnotationAtts.axes3D.axesType = AnnotationAtts.axes3D.OutsideEdges  # ClosestTriad, FurthestTriad, OutsideEdges, StaticTriad, StaticEdges
    AnnotationAtts.axes3D.triadFlag = 0
    AnnotationAtts.axes3D.bboxFlag = axvis
    AnnotationAtts.axes3D.xAxis.title.visible = 0
    AnnotationAtts.axes3D.xAxis.title.font.font = AnnotationAtts.axes3D.xAxis.title.font.Courier  # Arial, Courier, Times
    AnnotationAtts.axes3D.xAxis.title.font.scale = 1
    AnnotationAtts.axes3D.xAxis.title.font.useForegroundColor = 0
    AnnotationAtts.axes3D.xAxis.title.font.color = (192, 192, 192, 255)
    AnnotationAtts.axes3D.xAxis.title.font.bold = 1
    AnnotationAtts.axes3D.xAxis.title.font.italic = 1
    AnnotationAtts.axes3D.xAxis.title.userTitle = 0
    AnnotationAtts.axes3D.xAxis.title.userUnits = 0
    AnnotationAtts.axes3D.xAxis.title.title = "X"
    AnnotationAtts.axes3D.xAxis.title.units = ""
    AnnotationAtts.axes3D.xAxis.label.visible = 0
    AnnotationAtts.axes3D.xAxis.label.font.font = AnnotationAtts.axes3D.xAxis.label.font.Arial  # Arial, Courier, Times
    AnnotationAtts.axes3D.xAxis.label.font.scale = 1
    AnnotationAtts.axes3D.xAxis.label.font.useForegroundColor = 1
    AnnotationAtts.axes3D.xAxis.label.font.color = (0, 0, 0, 255)
    AnnotationAtts.axes3D.xAxis.label.font.bold = 0
    AnnotationAtts.axes3D.xAxis.label.font.italic = 0
    AnnotationAtts.axes3D.xAxis.label.scaling = 0
    AnnotationAtts.axes3D.xAxis.tickMarks.visible = axvis
    #AnnotationAtts.axes3D.xAxis.tickMarks.majorMinimum = -4
    #AnnotationAtts.axes3D.xAxis.tickMarks.majorMaximum = 4
    #AnnotationAtts.axes3D.xAxis.tickMarks.minorSpacing = 1e-05
    #AnnotationAtts.axes3D.xAxis.tickMarks.majorSpacing = 1e-06
    AnnotationAtts.axes3D.xAxis.grid = 0
    AnnotationAtts.axes3D.yAxis.title.visible = 0
    AnnotationAtts.axes3D.yAxis.title.font.font = AnnotationAtts.axes3D.yAxis.title.font.Courier  # Arial, Courier, Times
    AnnotationAtts.axes3D.yAxis.title.font.scale = 1
    AnnotationAtts.axes3D.yAxis.title.font.useForegroundColor = 1
    AnnotationAtts.axes3D.yAxis.title.font.color = (0, 0, 0, 255)
    AnnotationAtts.axes3D.yAxis.title.font.bold = 1
    AnnotationAtts.axes3D.yAxis.title.font.italic = 1
    AnnotationAtts.axes3D.yAxis.title.userTitle = 1
    AnnotationAtts.axes3D.yAxis.title.userUnits = 0
    AnnotationAtts.axes3D.yAxis.title.title = "Y"
    AnnotationAtts.axes3D.yAxis.title.units = ""
    AnnotationAtts.axes3D.yAxis.label.visible = 0
    AnnotationAtts.axes3D.yAxis.label.font.font = AnnotationAtts.axes3D.yAxis.label.font.Courier  # Arial, Courier, Times
    AnnotationAtts.axes3D.yAxis.label.font.scale = 1
    AnnotationAtts.axes3D.yAxis.label.font.useForegroundColor = 1
    AnnotationAtts.axes3D.yAxis.label.font.color = (0, 0, 0, 255)
    AnnotationAtts.axes3D.yAxis.label.font.bold = 1
    AnnotationAtts.axes3D.yAxis.label.font.italic = 1
    AnnotationAtts.axes3D.yAxis.label.scaling = 0
    AnnotationAtts.axes3D.yAxis.tickMarks.visible = axvis
    #AnnotationAtts.axes3D.yAxis.tickMarks.majorMinimum = -4e-06
    #AnnotationAtts.axes3D.yAxis.tickMarks.majorMaximum = 4e-06
    #AnnotationAtts.axes3D.yAxis.tickMarks.minorSpacing = 1e-05
    #AnnotationAtts.axes3D.yAxis.tickMarks.majorSpacing = 1e-06
    AnnotationAtts.axes3D.yAxis.grid = 0
    AnnotationAtts.axes3D.zAxis.title.visible = 0
    AnnotationAtts.axes3D.zAxis.title.font.font = AnnotationAtts.axes3D.zAxis.title.font.Courier  # Arial, Courier, Times
    AnnotationAtts.axes3D.zAxis.title.font.scale = 1
    AnnotationAtts.axes3D.zAxis.title.font.useForegroundColor = 1
    AnnotationAtts.axes3D.zAxis.title.font.color = (0, 0, 0, 255)
    AnnotationAtts.axes3D.zAxis.title.font.bold = 1
    AnnotationAtts.axes3D.zAxis.title.font.italic = 1
    AnnotationAtts.axes3D.zAxis.title.userTitle = 1
    AnnotationAtts.axes3D.zAxis.title.userUnits = 0
    AnnotationAtts.axes3D.zAxis.title.title = "Z"
    AnnotationAtts.axes3D.zAxis.title.units = ""
    AnnotationAtts.axes3D.zAxis.label.visible = 0
    AnnotationAtts.axes3D.zAxis.label.font.font = AnnotationAtts.axes3D.zAxis.label.font.Courier  # Arial, Courier, Times
    AnnotationAtts.axes3D.zAxis.label.font.scale = 1
    AnnotationAtts.axes3D.zAxis.label.font.useForegroundColor = 1
    AnnotationAtts.axes3D.zAxis.label.font.color = (0, 0, 0, 255)
    AnnotationAtts.axes3D.zAxis.label.font.bold = 0
    AnnotationAtts.axes3D.zAxis.label.font.italic = 0
    AnnotationAtts.axes3D.zAxis.label.scaling = 0
    AnnotationAtts.axes3D.zAxis.tickMarks.visible = axvis
    AnnotationAtts.axes3D.zAxis.tickMarks.majorMinimum = 0
    AnnotationAtts.axes3D.zAxis.tickMarks.majorMaximum = 1
    AnnotationAtts.axes3D.zAxis.tickMarks.minorSpacing = 0.02
    AnnotationAtts.axes3D.zAxis.tickMarks.majorSpacing = 0.2
    AnnotationAtts.axes3D.zAxis.grid = 0
    AnnotationAtts.axes3D.setBBoxLocation = 0
    AnnotationAtts.axes3D.bboxLocation = (0, 1, 0, 1, 0, 1)
    AnnotationAtts.userInfoFlag = 0
    AnnotationAtts.databaseInfoFlag = 0
    AnnotationAtts.timeInfoFlag = 1
    AnnotationAtts.legendInfoFlag = 0
    AnnotationAtts.backgroundColor = (0, 0, 0, 255) if bg == 'k' else (255, 255, 255, 255)
    AnnotationAtts.foregroundColor = (255, 255, 255, 255) if bg == 'k' else (0, 0, 0, 255)
    AnnotationAtts.backgroundMode = AnnotationAtts.Solid  # Solid, Gradient, Image, ImageSphere
    SetAnnotationAttributes(AnnotationAtts)

def saveWin(fn, rx=1665, ry=1256):
    SaveWindowAtts = SaveWindowAttributes()
    SaveWindowAtts.family = 0
    SaveWindowAtts.fileName = fn
    SaveWindowAtts.format = SaveWindowAtts.PNG  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
    SaveWindowAtts.width = rx
    SaveWindowAtts.height = ry
    SaveWindowAtts.resConstraint = SaveWindowAtts.NoConstraint  # NoConstraint, EqualWidthHeight, ScreenProportions
    SetSaveWindowAttributes(SaveWindowAtts)
    SaveWindow()

def exportVTK(filename, variables=None):
    # Set the export database parameters
    ExportDBAtts = ExportDBAttributes()
    ExportDBAtts.allTimes = 0
    ExportDBAtts.db_type = 'VTK'
    ExportDBAtts.db_type_fullname = 'VTK_1.0'
    ExportDBAtts.filename = filename
    ExportDBAtts.dirname = './'
    ExportDBAtts.variables = () if variables is None else variables
    ExportDBAtts.opts.types = (0, 0)
    # Set the additional export options for the VTK writer
    ExportOpts = GetExportOptions("VTK")
    ExportOpts['Binary format'] = 1
    ExportOpts['XML format'] = 1
    # Export the database
    ExportDatabase(ExportDBAtts, ExportOpts)

def setCam2D():
    View2DAtts = View2DAttributes()
    View2DAtts.windowCoords = (bounds[4], bounds[5], bounds[0], bounds[1])
    View2DAtts.viewportCoords = (0.1, 0.9, 0.1, 0.9)
    View2DAtts.fullFrameActivationMode = View2DAtts.On  # On, Off, Auto
    View2DAtts.fullFrameAutoThreshold = 100
    View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
    View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
    View2DAtts.windowValid = 1
    SetView2D(View2DAtts)

def setAtts2D():
    AnnotationAtts = AnnotationAttributes()
    AnnotationAtts.axes2D.visible = 1
    AnnotationAtts.axes2D.autoSetTicks = 1
    AnnotationAtts.axes2D.autoSetScaling = 1
    AnnotationAtts.axes2D.lineWidth = 0
    AnnotationAtts.axes2D.tickLocation = AnnotationAtts.axes2D.Outside  # Inside, Outside, Both
    AnnotationAtts.axes2D.tickAxes = AnnotationAtts.axes2D.BottomLeft  # Off, Bottom, Left, BottomLeft, All
    AnnotationAtts.axes2D.xAxis.title.visible = 1
    AnnotationAtts.axes2D.xAxis.title.font.font = AnnotationAtts.axes2D.xAxis.title.font.Courier  # Arial, Courier, Times
    AnnotationAtts.axes2D.xAxis.title.font.scale = 1.5
    AnnotationAtts.axes2D.xAxis.title.font.useForegroundColor = 1
    AnnotationAtts.axes2D.xAxis.title.font.color = (0, 0, 0, 255)
    AnnotationAtts.axes2D.xAxis.title.font.bold = 1
    AnnotationAtts.axes2D.xAxis.title.font.italic = 1
    AnnotationAtts.axes2D.xAxis.title.userTitle = 1
    AnnotationAtts.axes2D.xAxis.title.userUnits = 0
    AnnotationAtts.axes2D.xAxis.title.title = "Z"
    AnnotationAtts.axes2D.xAxis.title.units = ""
    AnnotationAtts.axes2D.xAxis.label.visible = 1
    AnnotationAtts.axes2D.xAxis.label.font.font = AnnotationAtts.axes2D.xAxis.label.font.Courier  # Arial, Courier, Times
    AnnotationAtts.axes2D.xAxis.label.font.scale = 1.5
    AnnotationAtts.axes2D.xAxis.label.font.useForegroundColor = 1
    AnnotationAtts.axes2D.xAxis.label.font.color = (0, 0, 0, 255)
    AnnotationAtts.axes2D.xAxis.label.font.bold = 1
    AnnotationAtts.axes2D.xAxis.label.font.italic = 1
    AnnotationAtts.axes2D.xAxis.label.scaling = 0
    AnnotationAtts.axes2D.xAxis.tickMarks.visible = 1
    AnnotationAtts.axes2D.xAxis.tickMarks.majorMinimum = 0
    AnnotationAtts.axes2D.xAxis.tickMarks.majorMaximum = 1
    AnnotationAtts.axes2D.xAxis.tickMarks.minorSpacing = 0.02
    AnnotationAtts.axes2D.xAxis.tickMarks.majorSpacing = 0.2
    AnnotationAtts.axes2D.xAxis.grid = 0
    AnnotationAtts.axes2D.yAxis.title.visible = 1
    AnnotationAtts.axes2D.yAxis.title.font.font = AnnotationAtts.axes2D.yAxis.title.font.Courier  # Arial, Courier, Times
    AnnotationAtts.axes2D.yAxis.title.font.scale = 1.5
    AnnotationAtts.axes2D.yAxis.title.font.useForegroundColor = 1
    AnnotationAtts.axes2D.yAxis.title.font.color = (0, 0, 0, 255)
    AnnotationAtts.axes2D.yAxis.title.font.bold = 1
    AnnotationAtts.axes2D.yAxis.title.font.italic = 1
    AnnotationAtts.axes2D.yAxis.title.userTitle = 1
    AnnotationAtts.axes2D.yAxis.title.userUnits = 0
    AnnotationAtts.axes2D.yAxis.title.title = "X"
    AnnotationAtts.axes2D.yAxis.title.units = ""
    AnnotationAtts.axes2D.yAxis.label.visible = 1
    AnnotationAtts.axes2D.yAxis.label.font.font = AnnotationAtts.axes2D.yAxis.label.font.Courier  # Arial, Courier, Times
    AnnotationAtts.axes2D.yAxis.label.font.scale = 1.5
    AnnotationAtts.axes2D.yAxis.label.font.useForegroundColor = 1
    AnnotationAtts.axes2D.yAxis.label.font.color = (0, 0, 0, 255)
    AnnotationAtts.axes2D.yAxis.label.font.bold = 1
    AnnotationAtts.axes2D.yAxis.label.font.italic = 1
    AnnotationAtts.axes2D.yAxis.label.scaling = 0
    AnnotationAtts.axes2D.yAxis.tickMarks.visible = 1
    AnnotationAtts.axes2D.yAxis.tickMarks.majorMinimum = 0
    AnnotationAtts.axes2D.yAxis.tickMarks.majorMaximum = 1
    AnnotationAtts.axes2D.yAxis.tickMarks.minorSpacing = 0.02
    AnnotationAtts.axes2D.yAxis.tickMarks.majorSpacing = 0.2
    AnnotationAtts.axes2D.yAxis.grid = 0
    AnnotationAtts.userInfoFlag = 0
    AnnotationAtts.userInfoFont.font = AnnotationAtts.userInfoFont.Arial  # Arial, Courier, Times
    AnnotationAtts.userInfoFont.scale = 1
    AnnotationAtts.userInfoFont.useForegroundColor = 1
    AnnotationAtts.userInfoFont.color = (0, 0, 0, 255)
    AnnotationAtts.userInfoFont.bold = 0
    AnnotationAtts.userInfoFont.italic = 0
    AnnotationAtts.databaseInfoFlag = 0
    AnnotationAtts.timeInfoFlag = 1
    AnnotationAtts.databaseInfoFont.font = AnnotationAtts.databaseInfoFont.Arial  # Arial, Courier, Times
    AnnotationAtts.databaseInfoFont.scale = 1
    AnnotationAtts.databaseInfoFont.useForegroundColor = 1
    AnnotationAtts.databaseInfoFont.color = (0, 0, 0, 255)
    AnnotationAtts.databaseInfoFont.bold = 0
    AnnotationAtts.databaseInfoFont.italic = 0
    AnnotationAtts.databaseInfoExpansionMode = AnnotationAtts.File  # File, Directory, Full, Smart, SmartDirectory
    AnnotationAtts.databaseInfoTimeScale = 1
    AnnotationAtts.databaseInfoTimeOffset = 0
    AnnotationAtts.legendInfoFlag = 0
    AnnotationAtts.backgroundColor = (0, 0, 0, 255)
    AnnotationAtts.foregroundColor = (255, 255, 255, 255)
    AnnotationAtts.gradientBackgroundStyle = AnnotationAtts.Radial  # TopToBottom, BottomToTop, LeftToRight, RightToLeft, Radial
    AnnotationAtts.gradientColor1 = (0, 0, 255, 255)
    AnnotationAtts.gradientColor2 = (0, 0, 0, 255)
    AnnotationAtts.backgroundMode = AnnotationAtts.Solid  # Solid, Gradient, Image, ImageSphere
    AnnotationAtts.backgroundImage = ""
    AnnotationAtts.imageRepeatX = 1
    AnnotationAtts.imageRepeatY = 1
    AnnotationAtts.axesArray.visible = 1
    AnnotationAtts.axesArray.ticksVisible = 1
    AnnotationAtts.axesArray.autoSetTicks = 1
    AnnotationAtts.axesArray.autoSetScaling = 1
    AnnotationAtts.axesArray.lineWidth = 0
    AnnotationAtts.axesArray.axes.title.visible = 1
    AnnotationAtts.axesArray.axes.title.font.font = AnnotationAtts.axesArray.axes.title.font.Arial  # Arial, Courier, Times
    AnnotationAtts.axesArray.axes.title.font.scale = 1
    AnnotationAtts.axesArray.axes.title.font.useForegroundColor = 1
    AnnotationAtts.axesArray.axes.title.font.color = (0, 0, 0, 255)
    AnnotationAtts.axesArray.axes.title.font.bold = 0
    AnnotationAtts.axesArray.axes.title.font.italic = 0
    AnnotationAtts.axesArray.axes.title.userTitle = 0
    AnnotationAtts.axesArray.axes.title.userUnits = 0
    AnnotationAtts.axesArray.axes.title.title = ""
    AnnotationAtts.axesArray.axes.title.units = ""
    AnnotationAtts.axesArray.axes.label.visible = 1
    AnnotationAtts.axesArray.axes.label.font.font = AnnotationAtts.axesArray.axes.label.font.Arial  # Arial, Courier, Times
    AnnotationAtts.axesArray.axes.label.font.scale = 1
    AnnotationAtts.axesArray.axes.label.font.useForegroundColor = 1
    AnnotationAtts.axesArray.axes.label.font.color = (0, 0, 0, 255)
    AnnotationAtts.axesArray.axes.label.font.bold = 0
    AnnotationAtts.axesArray.axes.label.font.italic = 0
    AnnotationAtts.axesArray.axes.label.scaling = 0
    AnnotationAtts.axesArray.axes.tickMarks.visible = 1
    AnnotationAtts.axesArray.axes.tickMarks.majorMinimum = 0
    AnnotationAtts.axesArray.axes.tickMarks.majorMaximum = 1
    AnnotationAtts.axesArray.axes.tickMarks.minorSpacing = 0.02
    AnnotationAtts.axesArray.axes.tickMarks.majorSpacing = 0.2
    AnnotationAtts.axesArray.axes.grid = 0
    SetAnnotationAttributes(AnnotationAtts)


SetColorTable("black", scaledlut(0, 0, 0, 0, 0, 0))
SetColorTable("proton_ke", scaledlut(229, 0, 0, 255, 55, 0))
SetColorTable("carbon_ke", scaledlut(0, 215, 15, 255, 55, 0))
SetColorTable("electron_ke", scaledlut(0, 132, 255, 255, 55, 0))
SetColorTable("grayscale_ke", scaledlut(255, 255, 255, 255, 55, 0))
SetColorTable("cpe_ke", appendluts(["carbon_ke", "black", "proton_ke", "black", "electron_ke"]))
SetColorTable("proton_den", scaledlut(229, 0, 0, 255, 100, 100))
SetColorTable("carbon_den", scaledlut(0, 215, 15, 255, 100, 100))
SetColorTable("electron_den", scaledlut(0, 132, 255, 255, 75, 100))
SetColorTable("grayscale_den", scaledlut(255, 255, 255, 255, 75, 100))
SetColorTable("cpe_den", appendluts(["carbon_den", "black", "proton_den", "black", "electron_den"]))

#DefineScalarExpression("e_log_ke", "log10(<Electron-All/ke>)")
#DefineScalarExpression("c_log_ke", "log10(<Carbon-0/ke>)")
#DefineScalarExpression("p_log_ke", "log10(<Proton-0/ke>)")

lut = copylut(GetColorTable("caleblack"))
lut.RemoveControlPoints(6)
n = lut.GetNumControlPoints()
delta = 1.0/(n-1.0)
i = 0
while i < n:
    lut.GetControlPoints(i).position = i*delta
    i += 1
SetColorTable("caleblack_pck2", lut)


def GenerateSymGrayLUT(ma=0.0):
    newlut = ColorControlPointList()
    cp = ColorControlPoint()
    cp.colors = (159, 75, 0, 255)
    cp.position = 0.0
    newlut.AddControlPoints(cp)
    a = 0.3
    cp.colors = (159*a+150*(1-a), 75*a+150*(1-a), 150*(a-1), ma*255)
    cp.position = 0.35
    newlut.AddControlPoints(cp)
    cp.colors = (150, 150, 150, 0)
    cp.position = 0.5
    newlut.AddControlPoints(cp)
    cp.colors = (255*a+150*(1-a), 255*a+150*(1-a), 150*(a-1), ma*255)
    cp.position = 0.65
    newlut.AddControlPoints(cp)
    cp.colors = (255, 255, 0, 255)
    cp.position = 1.0
    newlut.AddControlPoints(cp)
    return newlut

SetColorTable("sym_gray", GenerateSymGrayLUT(0.15))
