
# counter for cycling various script params
try:
    pdall2_it
except NameError:
    pdall2_it = 0
    pdall2_im_id = 0
else:
    pdall2_it += 1
    pdall2_im_id += 1

AddPlot("Pseudocolor", "grid/E_magnitude", 1, 0)
SetActivePlots(0)
SetActivePlots(0)
SetActivePlots(0)

PseudocolorAtts = PseudocolorAttributes()
PseudocolorAtts.scaling = PseudocolorAtts.Linear  # Linear, Log, Skew
PseudocolorAtts.skewFactor = 1
PseudocolorAtts.limitsMode = PseudocolorAtts.OriginalData  # OriginalData, CurrentPlot
PseudocolorAtts.minFlag = 0
PseudocolorAtts.min = 0
PseudocolorAtts.maxFlag = 0
PseudocolorAtts.max = 1
PseudocolorAtts.centering = PseudocolorAtts.Natural  # Natural, Nodal, Zonal
PseudocolorAtts.colorTableName = "gray"
PseudocolorAtts.invertColorTable = 0
PseudocolorAtts.opacityType = PseudocolorAtts.Constant  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
PseudocolorAtts.opacityVariable = ""
PseudocolorAtts.opacity = 0.80
PseudocolorAtts.opacityVarMin = 0
PseudocolorAtts.opacityVarMax = 1
PseudocolorAtts.opacityVarMinFlag = 0
PseudocolorAtts.opacityVarMaxFlag = 0
PseudocolorAtts.pointSize = 0.05
PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
PseudocolorAtts.pointSizeVarEnabled = 0
PseudocolorAtts.pointSizeVar = "default"
PseudocolorAtts.pointSizePixels = 2
PseudocolorAtts.lineType = PseudocolorAtts.Line  # Line, Tube, Ribbon
PseudocolorAtts.lineStyle = PseudocolorAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
PseudocolorAtts.lineWidth = 0
PseudocolorAtts.tubeDisplayDensity = 10
PseudocolorAtts.tubeRadiusSizeType = PseudocolorAtts.FractionOfBBox  # Absolute, FractionOfBBox
PseudocolorAtts.tubeRadiusAbsolute = 0.125
PseudocolorAtts.tubeRadiusBBox = 0.005
PseudocolorAtts.varyTubeRadius = 0
PseudocolorAtts.varyTubeRadiusVariable = ""
PseudocolorAtts.varyTubeRadiusFactor = 10
PseudocolorAtts.endPointType = PseudocolorAtts.None  # None, Tails, Heads, Both
PseudocolorAtts.endPointStyle = PseudocolorAtts.Spheres  # Spheres, Cones
PseudocolorAtts.endPointRadiusSizeType = PseudocolorAtts.FractionOfBBox  # Absolute, FractionOfBBox
PseudocolorAtts.endPointRadiusAbsolute = 1
PseudocolorAtts.endPointRadiusBBox = 0.005
PseudocolorAtts.endPointRatio = 2
PseudocolorAtts.renderSurfaces = 1
PseudocolorAtts.renderWireframe = 0
PseudocolorAtts.renderPoints = 0
PseudocolorAtts.smoothingLevel = 0
PseudocolorAtts.legendFlag = 1
PseudocolorAtts.lightingFlag = 1
SetPlotOptions(PseudocolorAtts)

AddOperator("Transform", 0)
TransformAtts = TransformAttributes()
TransformAtts.doRotate = 1
TransformAtts.rotateOrigin = (0, 0, 0)
TransformAtts.rotateAxis = (0, 1, 0)
TransformAtts.rotateAmount = 90
TransformAtts.rotateType = TransformAtts.Deg  # Deg, Rad
TransformAtts.doScale = 1
TransformAtts.scaleOrigin = (0, 0, 0)
TransformAtts.scaleX = 1
TransformAtts.scaleY = 1
TransformAtts.scaleZ = -1
SetOperatorOptions(TransformAtts, 0)

AddOperator("Project", 0)
ProjectAtts = ProjectAttributes()
ProjectAtts.projectionType = ProjectAtts.XZCartesian  # ZYCartesian, XZCartesian, XYCartesian, XRCylindrical, YRCylindrical, ZRCylindrical
SetOperatorOptions(ProjectAtts, 0)

var = ['Electron-All', 'Carbon-0', 'Proton-0']
luts = ['ion_electrons', 'ion_carbon', 'ion_proton']

i = 0
while i < 3:
    AddPlot("Pseudocolor", var[i] + "/e_magnitude", 1, 0)
    SetActivePlots(i+1)
    SetActivePlots(i+1)
    SetActivePlots(i+1)

    AddOperator("DataBinning",0)
    DataBinningAtts = DataBinningAttributes()
    DataBinningAtts.numDimensions = DataBinningAtts.Two  # One, Two, Three
    DataBinningAtts.dim1BinBasedOn = DataBinningAtts.Z  # X, Y, Z, Variable
    DataBinningAtts.dim1NumBins = int(nz/dim_scale)
    DataBinningAtts.dim2BinBasedOn = DataBinningAtts.X  # X, Y, Z, Variable
    DataBinningAtts.dim2NumBins = int(nx/dim_scale)
    DataBinningAtts.reductionOperator = DataBinningAtts.Count  # Average, Minimum, Maximum, StandardDeviation, Variance, Sum, Count, RMS, PDF
    DataBinningAtts.emptyVal = -1
    DataBinningAtts.outputType = DataBinningAtts.OutputOnInputMesh  # OutputOnBins, OutputOnInputMesh
    SetOperatorOptions(DataBinningAtts, 0)

    # fix pcolor variable
    ChangeActivePlotsVar("operators/DataBinning/2D/" + var[i])

    PseudocolorAtts = PseudocolorAttributes()
    PseudocolorAtts.scaling = PseudocolorAtts.Linear  # Linear, Log, Skew
    PseudocolorAtts.skewFactor = 1
    PseudocolorAtts.limitsMode = PseudocolorAtts.OriginalData  # OriginalData, CurrentPlot
    PseudocolorAtts.minFlag = 0
    PseudocolorAtts.min = 0
    PseudocolorAtts.maxFlag = 0
    PseudocolorAtts.max = 1
    PseudocolorAtts.centering = PseudocolorAtts.Natural  # Natural, Nodal, Zonal
    PseudocolorAtts.colorTableName = luts[i]
    PseudocolorAtts.invertColorTable = 0
    PseudocolorAtts.opacityType = PseudocolorAtts.ColorTable  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
    PseudocolorAtts.opacityVariable = ""
    PseudocolorAtts.opacity = 0.75
    PseudocolorAtts.opacityVarMin = 0
    PseudocolorAtts.opacityVarMax = 1
    PseudocolorAtts.opacityVarMinFlag = 0
    PseudocolorAtts.opacityVarMaxFlag = 0
    PseudocolorAtts.pointSize = 0.05
    PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
    PseudocolorAtts.pointSizeVarEnabled = 0
    PseudocolorAtts.pointSizeVar = "default"
    PseudocolorAtts.pointSizePixels = 2
    PseudocolorAtts.renderSurfaces = 1
    PseudocolorAtts.renderWireframe = 0
    PseudocolorAtts.renderPoints = 0
    PseudocolorAtts.smoothingLevel = 0
    PseudocolorAtts.legendFlag = 1
    PseudocolorAtts.lightingFlag = 1
    SetPlotOptions(PseudocolorAtts)

    AddOperator("Transform", 0)
    TransformAtts = TransformAttributes()
    TransformAtts.doRotate = 1
    TransformAtts.rotateOrigin = (0, 0, 0)
    TransformAtts.rotateAxis = (0, 1, 0)
    TransformAtts.rotateAmount = 90
    TransformAtts.rotateType = TransformAtts.Deg  # Deg, Rad
    TransformAtts.doScale = 1
    TransformAtts.scaleOrigin = (0, 0, 0)
    TransformAtts.scaleX = 1
    TransformAtts.scaleY = 1
    TransformAtts.scaleZ = -1
    SetOperatorOptions(TransformAtts, 0)

    AddOperator("Project", 0)
    ProjectAtts = ProjectAttributes()
    ProjectAtts.projectionType = ProjectAtts.XZCartesian  # ZYCartesian, XZCartesian, XYCartesian, XRCylindrical, YRCylindrical, ZRCylindrical
    SetOperatorOptions(ProjectAtts, 0)

    i += 1

View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1e-08, 1.601e-05, -5e-06, 5e-06)
View2DAtts.viewportCoords = (0.1, 0.9, 0.1, 0.9)
View2DAtts.fullFrameActivationMode = View2DAtts.On  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)

# Logging for SetAnnotationObjectOptions is not implemented yet.
AnnotationAtts = AnnotationAttributes()
AnnotationAtts.axes2D.visible = 1
AnnotationAtts.axes2D.autoSetTicks = 1
AnnotationAtts.axes2D.autoSetScaling = 1
AnnotationAtts.axes2D.lineWidth = 0
AnnotationAtts.axes2D.tickLocation = AnnotationAtts.axes2D.Outside  # Inside, Outside, Both
AnnotationAtts.axes2D.tickAxes = AnnotationAtts.axes2D.BottomLeft  # Off, Bottom, Left, BottomLeft, All
AnnotationAtts.axes2D.xAxis.title.visible = 1
AnnotationAtts.axes2D.xAxis.title.font.font = AnnotationAtts.axes2D.xAxis.title.font.Courier  # Arial, Courier, Times
AnnotationAtts.axes2D.xAxis.title.font.scale = 1.5
AnnotationAtts.axes2D.xAxis.title.font.useForegroundColor = 1
AnnotationAtts.axes2D.xAxis.title.font.color = (0, 0, 0, 255)
AnnotationAtts.axes2D.xAxis.title.font.bold = 1
AnnotationAtts.axes2D.xAxis.title.font.italic = 1
AnnotationAtts.axes2D.xAxis.title.userTitle = 1
AnnotationAtts.axes2D.xAxis.title.userUnits = 0
AnnotationAtts.axes2D.xAxis.title.title = "Z"
AnnotationAtts.axes2D.xAxis.title.units = ""
AnnotationAtts.axes2D.xAxis.label.visible = 1
AnnotationAtts.axes2D.xAxis.label.font.font = AnnotationAtts.axes2D.xAxis.label.font.Courier  # Arial, Courier, Times
AnnotationAtts.axes2D.xAxis.label.font.scale = 1.5
AnnotationAtts.axes2D.xAxis.label.font.useForegroundColor = 1
AnnotationAtts.axes2D.xAxis.label.font.color = (0, 0, 0, 255)
AnnotationAtts.axes2D.xAxis.label.font.bold = 1
AnnotationAtts.axes2D.xAxis.label.font.italic = 1
AnnotationAtts.axes2D.xAxis.label.scaling = 0
AnnotationAtts.axes2D.xAxis.tickMarks.visible = 1
AnnotationAtts.axes2D.xAxis.tickMarks.majorMinimum = 0
AnnotationAtts.axes2D.xAxis.tickMarks.majorMaximum = 1
AnnotationAtts.axes2D.xAxis.tickMarks.minorSpacing = 0.02
AnnotationAtts.axes2D.xAxis.tickMarks.majorSpacing = 0.2
AnnotationAtts.axes2D.xAxis.grid = 0
AnnotationAtts.axes2D.yAxis.title.visible = 1
AnnotationAtts.axes2D.yAxis.title.font.font = AnnotationAtts.axes2D.yAxis.title.font.Courier  # Arial, Courier, Times
AnnotationAtts.axes2D.yAxis.title.font.scale = 1.5
AnnotationAtts.axes2D.yAxis.title.font.useForegroundColor = 1
AnnotationAtts.axes2D.yAxis.title.font.color = (0, 0, 0, 255)
AnnotationAtts.axes2D.yAxis.title.font.bold = 1
AnnotationAtts.axes2D.yAxis.title.font.italic = 1
AnnotationAtts.axes2D.yAxis.title.userTitle = 1
AnnotationAtts.axes2D.yAxis.title.userUnits = 0
AnnotationAtts.axes2D.yAxis.title.title = "X"
AnnotationAtts.axes2D.yAxis.title.units = ""
AnnotationAtts.axes2D.yAxis.label.visible = 1
AnnotationAtts.axes2D.yAxis.label.font.font = AnnotationAtts.axes2D.yAxis.label.font.Courier  # Arial, Courier, Times
AnnotationAtts.axes2D.yAxis.label.font.scale = 1.5
AnnotationAtts.axes2D.yAxis.label.font.useForegroundColor = 1
AnnotationAtts.axes2D.yAxis.label.font.color = (0, 0, 0, 255)
AnnotationAtts.axes2D.yAxis.label.font.bold = 1
AnnotationAtts.axes2D.yAxis.label.font.italic = 1
AnnotationAtts.axes2D.yAxis.label.scaling = 0
AnnotationAtts.axes2D.yAxis.tickMarks.visible = 1
AnnotationAtts.axes2D.yAxis.tickMarks.majorMinimum = 0
AnnotationAtts.axes2D.yAxis.tickMarks.majorMaximum = 1
AnnotationAtts.axes2D.yAxis.tickMarks.minorSpacing = 0.02
AnnotationAtts.axes2D.yAxis.tickMarks.majorSpacing = 0.2
AnnotationAtts.axes2D.yAxis.grid = 0
AnnotationAtts.userInfoFlag = 0
AnnotationAtts.userInfoFont.font = AnnotationAtts.userInfoFont.Arial  # Arial, Courier, Times
AnnotationAtts.userInfoFont.scale = 1
AnnotationAtts.userInfoFont.useForegroundColor = 1
AnnotationAtts.userInfoFont.color = (0, 0, 0, 255)
AnnotationAtts.userInfoFont.bold = 0
AnnotationAtts.userInfoFont.italic = 0
AnnotationAtts.databaseInfoFlag = 0
AnnotationAtts.timeInfoFlag = 1
AnnotationAtts.databaseInfoFont.font = AnnotationAtts.databaseInfoFont.Arial  # Arial, Courier, Times
AnnotationAtts.databaseInfoFont.scale = 1
AnnotationAtts.databaseInfoFont.useForegroundColor = 1
AnnotationAtts.databaseInfoFont.color = (0, 0, 0, 255)
AnnotationAtts.databaseInfoFont.bold = 0
AnnotationAtts.databaseInfoFont.italic = 0
AnnotationAtts.databaseInfoExpansionMode = AnnotationAtts.File  # File, Directory, Full, Smart, SmartDirectory
AnnotationAtts.databaseInfoTimeScale = 1
AnnotationAtts.databaseInfoTimeOffset = 0
AnnotationAtts.legendInfoFlag = 1
AnnotationAtts.backgroundColor = (0, 0, 0, 255)
AnnotationAtts.foregroundColor = (255, 255, 255, 255)
AnnotationAtts.gradientBackgroundStyle = AnnotationAtts.Radial  # TopToBottom, BottomToTop, LeftToRight, RightToLeft, Radial
AnnotationAtts.gradientColor1 = (0, 0, 255, 255)
AnnotationAtts.gradientColor2 = (0, 0, 0, 255)
AnnotationAtts.backgroundMode = AnnotationAtts.Solid  # Solid, Gradient, Image, ImageSphere
AnnotationAtts.backgroundImage = ""
AnnotationAtts.imageRepeatX = 1
AnnotationAtts.imageRepeatY = 1
AnnotationAtts.axesArray.visible = 1
AnnotationAtts.axesArray.ticksVisible = 1
AnnotationAtts.axesArray.autoSetTicks = 1
AnnotationAtts.axesArray.autoSetScaling = 1
AnnotationAtts.axesArray.lineWidth = 0
AnnotationAtts.axesArray.axes.title.visible = 1
AnnotationAtts.axesArray.axes.title.font.font = AnnotationAtts.axesArray.axes.title.font.Arial  # Arial, Courier, Times
AnnotationAtts.axesArray.axes.title.font.scale = 1
AnnotationAtts.axesArray.axes.title.font.useForegroundColor = 1
AnnotationAtts.axesArray.axes.title.font.color = (0, 0, 0, 255)
AnnotationAtts.axesArray.axes.title.font.bold = 0
AnnotationAtts.axesArray.axes.title.font.italic = 0
AnnotationAtts.axesArray.axes.title.userTitle = 0
AnnotationAtts.axesArray.axes.title.userUnits = 0
AnnotationAtts.axesArray.axes.title.title = ""
AnnotationAtts.axesArray.axes.title.units = ""
AnnotationAtts.axesArray.axes.label.visible = 1
AnnotationAtts.axesArray.axes.label.font.font = AnnotationAtts.axesArray.axes.label.font.Arial  # Arial, Courier, Times
AnnotationAtts.axesArray.axes.label.font.scale = 1
AnnotationAtts.axesArray.axes.label.font.useForegroundColor = 1
AnnotationAtts.axesArray.axes.label.font.color = (0, 0, 0, 255)
AnnotationAtts.axesArray.axes.label.font.bold = 0
AnnotationAtts.axesArray.axes.label.font.italic = 0
AnnotationAtts.axesArray.axes.label.scaling = 0
AnnotationAtts.axesArray.axes.tickMarks.visible = 1
AnnotationAtts.axesArray.axes.tickMarks.majorMinimum = 0
AnnotationAtts.axesArray.axes.tickMarks.majorMaximum = 1
AnnotationAtts.axesArray.axes.tickMarks.minorSpacing = 0.02
AnnotationAtts.axesArray.axes.tickMarks.majorSpacing = 0.2
AnnotationAtts.axesArray.axes.grid = 0
SetAnnotationAttributes(AnnotationAtts)

# add color bars
Courier = 1
Values = 1
HorizontalBottom =3

# e
o = GetAnnotationObject(GetPlotList().GetPlots(1).plotName)
o.active = 0
o.managePosition = 0
o.position = (0.685, 0.7)
o.xScale = 0.7
o.yScale = 0.3
o.textColor = (0, 0, 0, 255)
o.useForegroundForTextColor = 1
o.drawBoundingBox = 0
o.boundingBoxColor = (0, 0, 0, 50)
o.numberFormat = "%6.2e"
o.fontFamily = Courier  # Arial, Courier, Times
o.fontBold = 1
o.fontItalic = 1
o.fontShadow = 0
o.fontHeight = 0.06
o.drawLabels = Values # None, Values, Labels, Both
o.drawTitle = 0
o.drawMinMax = 0
o.orientation = HorizontalBottom  # VerticalRight, VerticalLeft, HorizontalTop, HorizontalBottom
o.controlTicks = 1
o.numTicks = 3
o.minMaxInclusive = 1
#
o = CreateAnnotationObject('Text2D')
o.visible = 1
o.active = 0
o.position = (0.66, 0.675)
o.height = 0.02
o.textColor = (255, 255, 255, 255)
o.useForegroundForTextColor = 1
o.text = "e"
o.fontFamily = Courier  # Arial, Courier, Times
o.fontBold = 1
o.fontItalic = 1
o.fontShadow = 0

# C
o = GetAnnotationObject(GetPlotList().GetPlots(2).plotName)
o.active = 0
o.managePosition = 0
o.position = (0.685, 0.76)
o.xScale = 0.7
o.yScale = 0.3
o.textColor = (0, 0, 0, 255)
o.useForegroundForTextColor = 1
o.drawBoundingBox = 0
o.boundingBoxColor = (0, 0, 0, 50)
o.numberFormat = "%6.2e"
o.fontFamily = Courier  # Arial, Courier, Times
o.fontBold = 1
o.fontItalic = 1
o.fontShadow = 0
o.fontHeight = 0.06
o.drawLabels = Values # None, Values, Labels, Both
o.drawTitle = 0
o.drawMinMax = 0
o.orientation = HorizontalBottom  # VerticalRight, VerticalLeft, HorizontalTop, HorizontalBottom
o.controlTicks = 1
o.numTicks = 3
o.minMaxInclusive = 1
#
o = CreateAnnotationObject('Text2D')
o.visible = 1
o.active = 0
o.position = (0.66, 0.74)
o.height = 0.02
o.textColor = (255, 255, 255, 255)
o.useForegroundForTextColor = 1
o.text = "C"
o.fontFamily = Courier  # Arial, Courier, Times
o.fontBold = 1
o.fontItalic = 1
o.fontShadow = 0

# P
o = GetAnnotationObject(GetPlotList().GetPlots(3).plotName)
o.active = 0
o.managePosition = 0
o.position = (0.685, 0.82)
o.xScale = 0.7
o.yScale = 0.3
o.textColor = (0, 0, 0, 255)
o.useForegroundForTextColor = 1
o.drawBoundingBox = 0
o.boundingBoxColor = (0, 0, 0, 50)
o.numberFormat = "%6.2e"
o.fontFamily = Courier  # Arial, Courier, Times
o.fontBold = 1
o.fontItalic = 1
o.fontShadow = 0
o.fontHeight = 0.06
o.drawLabels = Values # None, Values, Labels, Both
o.drawTitle = 0
o.drawMinMax = 0
o.orientation = HorizontalBottom  # VerticalRight, VerticalLeft, HorizontalTop, HorizontalBottom
o.controlTicks = 1
o.numTicks = 3
o.minMaxInclusive = 1
#
o = CreateAnnotationObject('Text2D')
o.visible = 1
o.active = 0
o.position = (0.66, 0.8)
o.height = 0.02
o.textColor = (255, 255, 255, 255)
o.useForegroundForTextColor = 1
o.text = "H"
o.fontFamily = Courier  # Arial, Courier, Times
o.fontBold = 1
o.fontItalic = 1
o.fontShadow = 0

# |E|
o = GetAnnotationObject(GetPlotList().GetPlots(0).plotName)
o.active = 0
o.managePosition = 0
o.position = (0.685, 0.88)
o.xScale = 0.7
o.yScale = 0.3
o.textColor = (0, 0, 0, 255)
o.useForegroundForTextColor = 1
o.drawBoundingBox = 0
o.boundingBoxColor = (255, 255, 255, 50)
o.numberFormat = "%6.2e"
o.fontFamily = Courier  # Arial, Courier, Times
o.fontBold = 1
o.fontItalic = 1
o.fontShadow = 0
o.fontHeight = 0.06
o.drawLabels = Values # None, Values, Labels, Both
o.drawTitle = 0
o.drawMinMax = 0
o.orientation = HorizontalBottom  # VerticalRight, VerticalLeft, HorizontalTop, HorizontalBottom
o.controlTicks = 1
o.numTicks = 3
o.minMaxInclusive = 1
#
o = CreateAnnotationObject('Text2D')
o.visible = 1
o.active = 0
o.position = (0.65, 0.86)
o.height = 0.02
o.textColor = (255, 255, 255, 255)
o.useForegroundForTextColor = 1
o.text = "|E|"
o.fontFamily = Courier  # Arial, Courier, Times
o.fontBold = 1
o.fontItalic = 1
o.fontShadow = 0

# t
o = CreateAnnotationObject('Text2D')
o.visible = 1
o.active = 1
o.position = (0.72, 0.92)
o.height = 0.03
o.textColor = (255, 255, 255, 255)
o.useForegroundForTextColor = 1
o.text = "t=$time"
o.fontFamily = Courier  # Arial, Courier, Times
o.fontBold = 1
o.fontItalic = 1
o.fontShadow = 0

# title
o = CreateAnnotationObject('Text2D')
o.visible = 1
o.active = 0
o.position = (0.1, 0.92)
o.height = 0.04
o.textColor = (255, 255, 255, 255)
o.useForegroundForTextColor = 1
o.text = "Density"
o.fontFamily = Courier  # Arial, Courier, Times
o.fontBold = 1
o.fontItalic = 1
o.fontShadow = 0

# render
DrawPlots()

# save the window
SaveWindowAtts = SaveWindowAttributes()
SaveWindowAtts.family = 0
SaveWindowAtts.fileName = 'All-part-den-2d-%04d.png'%(pdall2_im_id)
SaveWindowAtts.format = SaveWindowAtts.PNG  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
SaveWindowAtts.width = 1920
SaveWindowAtts.height = 1080
SaveWindowAtts.resConstraint = SaveWindowAtts.NoConstraint  # NoConstraint, EqualWidthHeight, ScreenProportions
SetSaveWindowAttributes(SaveWindowAtts)
SaveWindow()

DeleteAllPlots()

nms = GetAnnotationObjectNames()
for nm in nms:
  GetAnnotationObject(nm).Delete()
