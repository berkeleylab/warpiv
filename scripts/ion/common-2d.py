
def copylut(lut):
    newlut = ColorControlPointList()
    ncp = ColorControlPoint()
    n = lut.GetNumControlPoints()
    i=0
    while i < n:
        cp = lut.GetControlPoints(i)
        newlut.AddControlPoints(cp)
        i += 1
    return newlut

# luts
lut = copylut(GetColorTable("Blues"))
n = lut.GetNumControlPoints()
lut.RemoveControlPoints(n-1)
lut.RemoveControlPoints(0)
lut.RemoveControlPoints(0)
lut.RemoveControlPoints(0)
n = lut.GetNumControlPoints()
delta = 1.0/(n-1.0)
i = 0
while i < n:
    p = i*delta
    c = lut.GetControlPoints(i).colors
    lut.GetControlPoints(i).colors = (c[0], c[1], c[2], p*200)
    lut.GetControlPoints(i).position = p
    i += 1
c = lut.GetControlPoints(1).colors
lut.GetControlPoints(0).colors = (c[0], c[1], c[2], 0.0)
SetColorTable("ion_electrons", lut)

lut = copylut(GetColorTable("RdPu"))
n = lut.GetNumControlPoints()
lut.RemoveControlPoints(n-1)
lut.RemoveControlPoints(0)
lut.RemoveControlPoints(0)
lut.RemoveControlPoints(0)
lut.RemoveControlPoints(0)
n = lut.GetNumControlPoints()
delta = 1.0/(n-1.0)
i = 0
while i < n:
    p = i*delta
    a = 230 if p > 0.40 else p*230
    c = lut.GetControlPoints(i).colors
    lut.GetControlPoints(i).colors = (c[0], c[1], c[2], a)
    lut.GetControlPoints(i).position = p
    i += 1
c = lut.GetControlPoints(1).colors
lut.GetControlPoints(0).colors = (c[0], c[1], c[2], 0.0)
SetColorTable("ion_carbon", lut)

lut = copylut(GetColorTable("YlOrRd"))
lut.RemoveControlPoints(0)
lut.RemoveControlPoints(0)
lut.RemoveControlPoints(0)
n = lut.GetNumControlPoints()
delta = 1.0/(n-1.0)
i = 0
while i < n:
    p = i*delta
    a = 230 if p > 0.40 else p*230
    c = lut.GetControlPoints(i).colors
    lut.GetControlPoints(i).colors = (c[0], c[1], c[2], a)
    lut.GetControlPoints(i).position = p
    i += 1
c = lut.GetControlPoints(1).colors
lut.GetControlPoints(0).colors = (c[0], c[1], c[2], 0.0)
SetColorTable("ion_proton", lut)

lut = copylut(GetColorTable("caleblack"))
lut.RemoveControlPoints(6)
n = lut.GetNumControlPoints()
delta = 1.0/(n-1.0)
i = 0
while i < n:
    lut.GetControlPoints(i).position = i*delta
    i += 1
SetColorTable("caleblack_pck2", lut)
