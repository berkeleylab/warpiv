from WarpIVSpeciesUtil import getSpecies
import sys
import numpy
import warp

def PartStats(specName, varName):
    label = specName + '/' + varName
    species = getSpecies(specName)
    getter = getattr(species, 'get'+varName)
    data = getter(gather=0)
    n = parallel.globalsum(data.size)
    mn = parallel.globalmin(data)
    mx = parallel.globalmax(data)
    avg = parallel.globalave(data)
    var = parallel.globalvar(data)
    if parallel.get_rank() == 0:
        sys.stderr.write( \
            'stats : %s = [%d, %g, %g, %g, %g]\n'%( \
            label, n, mn, mx, avg, var))
    return

def GridStats(varName):
    solver = warp.getregisteredsolver()
    varmd = solver.dict_of_grids[varName]
    varget = getattr(solver, varmd['getter'])
    data = varget(0,0)
    mn = parallel.globalmin(data)
    mx = parallel.globalmax(data)
    avg = parallel.globalave(data)
    var = parallel.globalvar(data)
    if parallel.get_rank() == 0:
        sys.stderr.write( \
            'stats : %s = [%g, %g, %g, %g]\n'%( \
            varName, mn, mx, avg, var))
    return
