
try:
    pcxy_it
except NameError:
    pcxy_it = 0
else:
    pcxy_it += 1

spec = ['Electron-All', 'Carbon-0', 'Proton-0']
expr = ['Electron-All/ke', 'Carbon-0/ke', 'Proton-0/ke']
op_out = ['den', 'ke']
#lut = ['electron_den', 'carbon_den', 'proton_den']
#lut = ['grayscale_den', 'grayscale_ke']
lut = ['gray', 'gray']

i = 0
nspec = len(spec)
while i < nspec:
    active_spec = spec[i]
    active_expr = expr[i]
    j = 0
    while j < 2:
        AddPlot("Pseudocolor", active_spec + "/e_magnitude", 1, 0)

        AddOperator("DataBinning",0)

        # fix pcolor variable
        ChangeActivePlotsVar("operators/DataBinning/2D/" + active_spec)

        DataBinningAtts = DataBinningAttributes()

        op = [DataBinningAtts.Count, DataBinningAtts.Average]
        active_op = op[j]

        res = 800
        DataBinningAtts.numDimensions = DataBinningAtts.Two  # One, Two, Three
        DataBinningAtts.dim1BinBasedOn = DataBinningAtts.Variable  # X, Y, Z, Variable
        DataBinningAtts.dim1Var = active_spec + "/y"
        DataBinningAtts.dim1NumBins = res #int(ny/dim_scale)
        DataBinningAtts.dim1SpecifyRange = 1
        DataBinningAtts.dim1MinRange = bounds[2]
        DataBinningAtts.dim1MaxRange = bounds[3]
        DataBinningAtts.dim2BinBasedOn = DataBinningAtts.Variable  # X, Y, Z, Variable
        DataBinningAtts.dim2Var = active_spec + "/x"
        DataBinningAtts.dim2NumBins = res #int(nx/dim_scale)
        DataBinningAtts.dim2SpecifyRange = 1
        DataBinningAtts.dim2MinRange = bounds[0]
        DataBinningAtts.dim2MaxRange = bounds[1]
        DataBinningAtts.reductionOperator = active_op  # Average, Minimum, Maximum, StandardDeviation, Variance, Sum, Count, RMS, PDF
        DataBinningAtts.varForReduction = active_expr
        DataBinningAtts.emptyVal = 0
        DataBinningAtts.outputType = DataBinningAtts.OutputOnBins  # OutputOnBins, OutputOnInputMesh
        DataBinningAtts.emptyVal = 0
        SetOperatorOptions(DataBinningAtts, 0)

        # cell to node
        AddOperator("DualMesh", 0)

        PseudocolorAtts = PseudocolorAttributes()
        PseudocolorAtts.limitsMode = PseudocolorAtts.OriginalData  # OriginalData, CurrentPlot
        PseudocolorAtts.centering = PseudocolorAtts.Natural  # Natural, Nodal, Zonal
        #PseudocolorAtts.colorTableName = "caleblack_pck2"
        PseudocolorAtts.colorTableName = lut[j]
        PseudocolorAtts.minFlag = 0
        PseudocolorAtts.min = 0
        PseudocolorAtts.maxFlag = 0
        PseudocolorAtts.max = 400
        SetPlotOptions(PseudocolorAtts)

        DrawPlots()

        if binImages:
            setCam2D()
            setAtts2D()

            DrawPlots()
            saveWin('%s-pc-xy-%s-2d-%04d.png'%(active_spec, op_out[j], pcxy_it), 1920, 1080)

        exportVTK(filename='%s-pc-xy-%s-2d-%04d'%(active_spec, op_out[j], pcxy_it))

        DeleteAllPlots()

        j += 1
    i += 1
