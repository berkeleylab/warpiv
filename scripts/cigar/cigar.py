import os
#import sys
#import argparse
import math
#import numpy
import warpoptions
warpoptions.lskipoptions = 1
import warp
import extpart
#import em3dsolver
#import boosted_frame
#import species
#import PRpickle as PR
#import PWpickle as PW
from WarpIVSimulation import WarpIVSimulation
from WarpIVFilteredSpecies import WarpIVSpeciesFilters

#----------------------------------------------------------------------------
def NewWarpIVSimulation(args=[]):
    """
    factory that creates an obect implementing WarpIVSimulation
    class interface
    """
    return CigarSimulation(args)

#----------------------------------------------------------------------------
class CigarSimulation(WarpIVSimulation):
    """
    Implementation of the WarpIVSimulation class for two color
    lpa sim example.
    """
    def __init__(self,args=[]):
        global bigRun
        global runDim

        # call base class constructor
        WarpIVSimulation.__init__(self,args)

        renderingScripts = {
            'max(v(x,y))' : 'render-max-v.py',
            'binning bv'  : 'render-binning-bv.py',
            'scatter bv'  : 'render-scatter-bv.py',
            'volume phi'  : 'render-volume-phi.py',
            'particle v'  : 'render-particle-v.py',
            }

        for key,fileName in renderingScripts.iteritems():
            self.AddRenderScript(key, fileName)

    #------------------------------------------------------------------------
    def Initialize(self):
        """
        Setup IC and start the simulation.
        """
        # call scientist provided script to intialize warp
        initcigar()

        # call default implementation
        WarpIVSimulation.Initialize(self)

        print warp.getregisteredsolver().dict_of_grids
        print 'initialization complete'
        return


#-----------------------------------------------------------------------------
def initcigar():
    """
    Setup IC and start the simulation, but don't run it yet.
    """
    # --- Set four-character run id, comment lines, user's name.
    warp.top.pline2   = "Example 3D beam in a FODO lattice"
    warp.top.pline1   = "S-G cigar beam. 64x64x256"
    warp.top.runmaker = "David P. Grote"

    # --- Invoke setup routine - it is needed to created a cgm file for plots
    #warp.setup()

    # --- Create the beam species
    beam = warp.Species(type=warp.Potassium,charge_state=+1,name="Beam species")

    # --- Set input parameters describing the beam, 72 to 17.
    beam.b0       = 15.358933450767e-3
    beam.a0       =  8.6379155933081e-3
    beam.x0       = 3.*warp.mm
    beam.emit     = 51.700897052724e-6
    beam.ap0      = 0.e0
    beam.bp0      = 0.e0
    beam.ibeam    = 2.e-03
    beam.vbeam    = 0.e0
    beam.ekin     = 80.e3
    beam.aion     = beam.type.A
    beam.zion     = beam.charge_state
    warp.top.lrelativ = warp.false
    warp.top.derivqty()
    beam.vthz     = .5e0*beam.vbeam*beam.emit/math.sqrt(beam.a0*beam.b0) # Vthz ~ Vthperp

    # +++ Set up arrays describing lattice.
    # --- Set temp variables.
    hlp     = 36.0e-2  # half lattice period length
    piperad = 3.445e-2 # pipe radius
    quadlen = 11.e-2   # quadrupole length
    gaplen = 4.*warp.cm
    rodlen = quadlen + gaplen
    dbdx    = .949/quadlen

    # --- Set general lattice variables.
    warp.top.tunelen   = 2.e0*hlp
    warp.env.zl        = -hlp*2
    warp.env.zu        = -warp.env.zl
    warp.env.dzenv     = warp.top.tunelen/100.e0

    # --- Set up quadrupoles
    warp.addnewquad(zs= -quadlen/2.,
           ze= +quadlen/2.,
           db=-dbdx,ap=piperad)
    warp.addnewquad(zs=hlp - quadlen/2.,
           ze=hlp + quadlen/2.,
           db=+dbdx,ap=piperad)
    warp.addnewquad(zs=2.*hlp - quadlen/2.,
           ze=2.*hlp + quadlen/2.,
           db=-dbdx,ap=piperad)
    warp.top.zlatstrt  = 0.
    warp.top.zlatperi  = 2.e0*hlp

    # +++ Set input parameters describing the 3d simulation.
    warp.w3d.nx = 64/2
    warp.w3d.ny = 64/2
    warp.w3d.nz = 256/2
    steps_p_perd = 50
    warp.top.dt = (warp.top.tunelen/steps_p_perd)/beam.vbeam

    # --- Set to finite beam.
    warp.top.pbound0  = warp.top.pboundnz = warp.periodic
    warp.top.pboundxy = warp.absorb
    warp.w3d.xmmin = -piperad
    warp.w3d.xmmax =  piperad
    warp.w3d.ymmin = -piperad
    warp.w3d.ymmax =  piperad
    warp.w3d.zmmin = -hlp*2
    warp.w3d.zmmax = +hlp*2
    warp.top.prwall = piperad

    # --- Set pulse length.
    beam.zimin = warp.w3d.zmmin*.95/2.
    beam.zimax = warp.w3d.zmmax*.95/2.

    # --- Load Semi-Gaussian cigar beam.
    warp.top.npmax = 20000
    warp.w3d.distrbtn = "semigaus"
    warp.w3d.cigarld = warp.true
    warp.w3d.xrandom = "digitrev"
    warp.w3d.vtrandom = "digitrev"
    warp.w3d.vzrandom = "digitrev"
    warp.w3d.ldprfile = "polar"
    warp.w3d.cylinder = warp.false
    warp.top.straight = .8

    # --- set up field solver
    warp.w3d.l4symtry = warp.true
    warp.w3d.bound0 = warp.periodic
    warp.w3d.boundnz = warp.periodic
    warp.w3d.boundxy = warp.dirichlet

    solver = warp.MultiGrid3D()
    warp.registersolver(solver)

    pipe = warp.ZCylinderOut(piperad,4.,voltage=0.)
    warp.installconductors(pipe,dfill=warp.largepos)

    # --- Run the envelope solver to provide data used to initialize particles.
    warp.package("env")
    warp.generate()
    warp.step()

    # --- Generate the PIC code (allocate storage, load ptcls, t=0 plots, etc.).
    warp.package("w3d")
    warp.generate()

    return
