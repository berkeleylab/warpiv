#!python
import os
import sys
import argparse
import time
from WarpIVUtil import pError,pDebug,pStatus,VisItEnv
env = VisItEnv()
from WarpIVCLI import WarpIVCLI

#-----------------------------------------------------------------------------
if __name__ == "__main__":

    # parse command line args
    ap = argparse.ArgumentParser(usage=argparse.SUPPRESS,prog='',add_help=False)
    ap.add_argument('--help',default=False,action='store_true')
    opts = vars(ap.parse_known_args(sys.argv)[0])
    if opts['help']:
        pStatus('WarpIVCLIMain\nUsage:\n\n%s'%(WarpIVCLI.GetCommandLineHelp()))
        sys.exit(0)

    # create and run CLI/viewer
    cli = WarpIVCLI(sys.argv)
    cli.Initialize()
    status = cli.EventLoop()
    cli.Finalize()

    sys.exit(status)
