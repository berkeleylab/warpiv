class WarpIVDataSource(object):
    """Contains a mesh and a particle source object"""
    def __init__(self, psrc, msrc):
        self._ParticleSource = psrc
        self._MeshSource = msrc
        return

    def GetMeshSource(self):
        """Get the WarpIVMeshSource instance"""
        return self._MeshSource

    def GetParticleSource(self):
        """Get the WarpIVParticleSource instance"""
        return self._ParticleSource

    def GetTime(self):
        """return active time"""
        return 0.0

    def GetTimeStep(self):
        """return the active time step"""
        return 0

    def DecodeDataSpec(self, varid):
        """Given a data spec break it into it constituent parts"""
        tok = varid.split('/')
        ntok = len(tok)
        if ntok == 2:
            meshName = tok[0]
            varName = tok[1]
            srcMeshName = ''
            derivedQty = False
            opName = ''
        elif ntok == 3:
            meshName = tok[0]
            varName = ''
            srcMeshName = tok[1]
            derivedQty = True
            opName = tok[2]
        elif ntok == 4:
            meshName = tok[0]
            varName = tok[3]
            srcMeshName = tok[1]
            derivedQty = True
            opName = tok[2]
        else:
            pError('Invalid naming scheme %s'%(varid))
            raise RunTimeError('Invalid data spec "%s"'%(varid))
        return meshName, varName, srcMeshName, derivedQty, opName
