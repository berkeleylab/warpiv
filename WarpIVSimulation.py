import os
import sys
import argparse
import warp
import numpy as np
import itertools
from WarpIVUtil import pError,pDebug,pStatus,Struct
from WarpIVWarpDataSource import WarpIVWarpDataSource
from WarpIVArrayIO import writeArray

#############################################################################
def NewWarpIVSimulation(args=[]):
    """
    example of the user supplied factory method that WarpIV uses
    to create an object instance that implements WarVisItSimulation
    interface. args are command line arguments perhaps as obtained
    from sys.argv
    """
    return AWarpIVSimulationImplementaion(args)

#############################################################################
class WarpIVSimulation(object):
    """
    WarpIVSimulation

    Class describing interface used by WarpIV to control a running
    simulation. This class provides a default implementation for most
    of the methods. At a minimum a user will need to override Initialize
    to handle run specific warp iniziation. Users expose their implementation
    to WarpIV through a factory function which should defined at
    file scope in the file containing their WarpIVSimulation implementation
    and be named NewWarpIVSimulation(args), where args is a list of command
    line arguments (could be sys.argv). The run time is made aware of these
    through the --warp-script command line option, which should contain
    the path to the file. For example a user want to run a simulation called
    MySim, then they must at a minimum provide a file with the following:

    ##########################
    ##### begin MySim.py #####

    # 1: define the factory
    def NewWarpIVSimulation(args=[])
        return MySim(args)

    # 2: implement the simulation class interface
    class MySim(WarpIVSimulation):
        def __init__(self, args=[]):
            WarpIVSimulation.__init__(self,args)
            self.AddScript('plotV','plotV.py')
            # etc ...

        def Initialize(self):
            # setup and start warp here
            WarpIVSimulation.Initialize(self)

        # override other methods if needed.

    ##### end MySim.py #####
    ########################

    and start WarpIV with the command line options:
    --warp-script=/some/path/MySim.py --script-dir=/path/to/dir/of/plot.py
    """

    #-------------------------------------------------------------------------
    def __init__(self, args=[]):
        """
        Initialize the object and optionaly process common command
        line arguments.
        """
        self._ScriptDir = os.getenv('WARPIV_SCRIPT_DIR')
        if not self._ScriptDir:
            self._ScriptDir = ''
        self._Start = 0
        self._Stop = 500
        self._Step = 100
        self._Plot = 0
        self._IO = 0
        self._IODir = '.'
        self._IOPartHeader = 'warpIVPart.bop'
        self._IOPartSpec = None
        self._IOPartVars = None
        self._IOMeshVars = None
        self._IOMeshHeader = 'warpIVMesh.bov'
        self._CheckPoint = 0
        self._CheckPointFile = 'check_%d.warp'
        self._Scripts = None
        self._DataSource = None

        # parse command line args
        ap = argparse.ArgumentParser(usage=argparse.SUPPRESS,prog='WarpIVSimulation',add_help=False)
        ap.add_argument('--script-dir',type=str,default=self._ScriptDir)
        ap.add_argument('--start',type=int,default=self._Start)
        ap.add_argument('--stop',type=int,default=self._Stop)
        ap.add_argument('--step',type=int,default=self._Step)
        ap.add_argument('--plot',type=int,default=None)
        ap.add_argument('--io',type=int,default=self._IO)
        ap.add_argument('--io-dir',type=str,default=self._IODir)
        ap.add_argument('--io-part-header',type=str,default=self._IOPartHeader)
        ap.add_argument('--io-part-spec',type=str,default=self._IOPartSpec)
        ap.add_argument('--io-part-vars',type=str,default=self._IOPartVars)
        ap.add_argument('--io-mesh-header',type=str,default=self._IOMeshHeader)
        ap.add_argument('--io-mesh-vars',type=str,default=self._IOMeshVars)
        ap.add_argument('--check-point',type=int,default=self._CheckPoint)
        ap.add_argument('--check-point-file',type=str,default=self._CheckPointFile)
        opts = vars(ap.parse_known_args(args)[0])
        self._ScriptDir = os.path.abspath(opts['script_dir'])
        self._Start = opts['start']
        self._Stop = opts['stop']
        self._Step = opts['step']
        self._Plot = self._Step if opts['plot'] is None else opts['plot']
        self._IO = opts['io']
        self._IODir = opts['io_dir']
        self._IOPartHeader = opts['io_part_header']
        self._IOPartSpec = [] if opts['io_part_spec'] is None else opts['io_part_spec'].split(',')
        self._IOPartVars = [] if opts['io_part_vars'] is None else opts['io_part_vars'].split(',')
        self._IOMeshHeader = opts['io_mesh_header']
        self._IOMeshVars = [] if opts['io_mesh_vars'] is None else opts['io_mesh_vars'].split(',')
        self._CheckPoint = opts['check_point']
        self._CheckPointFile = opts['check_point_file']

        return

    #-------------------------------------------------------------------------
    @staticmethod
    def GetCommandLineHelp():
        """
        Return string listing command line options and environment
        vars(if any) one command per line. This is used in repsonse
        to --help
        """
        return \
           ("--script-dir     : : Path to directory containing vis scripts\n"
            "--start          : : Number of steps to take before visualization\n"
            "--stop           : : Step number to end the run at\n"
            "--step           : : Number of steps to advance with each update\n"
            "--plot           : : Number of steps between visualizations\n"
            "--io             : : Number of steps between mesh and particle I/O\n"
            "--io-dir         : : Where to write files\n"
            "--io-part-header : : Name of header file for mesh I/O\n"
            "--io-part-spec   : : List of particle species to write\n"
            "--io-part-vars   : : List of variables to write for each species\n"
            "--io-mesh-header : : Name of header file for mesh I/O\n"
            "--io-mesh-vars   : : List of varibes to write on the mesh\n"
            "--check-point    : : Number of steps between check point I/O\n"
            "--check-point-file : : File name to write check points. if present %d is replaced with step id.\n")

    #-------------------------------------------------------------------------
    def GetDataSource(self):
        """
        Return the simulation's data source. The default is the Warp
        data source, which will serve up warp simulation data.
        """
        if self._DataSource is None:
            self._DataSource = WarpIVWarpDataSource()
        return self._DataSource

    #------------------------------------------------------------------------
    def GetActiveScripts(self):
        """
        If the current time step should be visualized return a list
        of keys naming which rendering scripts should run. If the
        list is empty then nothing will be rendered this time step.

         scripts are executed in VisIt's CLI.
        """
        return []

    #------------------------------------------------------------------------
    def Advance(self):
        """
        Advance the simulation n steps, where n can be passed on the
        command line via --step option or by calling SetStepInterval.
        """
        warp.step(self._Step)
        return

    #------------------------------------------------------------------------
    def Continue(self):
        """
        Return False when the run is finished, and True otherwise.
        This implementation will run until the n'th simulation step
        where n can be passed on the command line via the --stop
        option or by calling SetStopIteration.
        """
        return warp.warp.top.it < self._Stop

    #------------------------------------------------------------------------
    def WriteData(self):
        """
        Handle output of data here.
        """
        if (warp.warp.top.it > 0) \
            and (self._IO > 0) \
            and ((warp.warp.top.it%self._IO) == 0):
            pStatus('Writing data')
            #self.WriteParticleData(self._IODir, self._IOPartSpec, self._IOPartVars)
            self.WriteMeshData()
        return True

    #------------------------------------------------------------------------
    def WriteCheckPoint(self):
        """
        Handle output of data here.
        """
        if (warp.warp.top.it > 0) \
            and (self._CheckPoint > 0) \
            and ((warp.warp.top.it % self._CheckPoint) == 0):
            pStatus('Writing checkpoint')
            #fn = self._CheckPointFile
            #if '%d' in fn:
            #    fn = fn%(warp.warp.top.it)
            warp.dump()
        return True

    #------------------------------------------------------------------------
    def Finalize(self):
        """
        This is called as part of normal shutdown. Clean up code can be
        insrted here. This implementation does nothing.
        """
        if (self._IO > 0) and (len(self._IOMeshVars)):
            self.WriteMeshHeader()
        return

    #------------------------------------------------------------------------
    def Initialize(self):
        """
        This will be called as part of start up. This method should be
        overriden to configure the simulation initial condition etc.

        This implementation takes n steps, where n may be passed on the
        command line via the --start option, or by calling SetStartIteration.
        """
        warp.step(self._Start)
        return

    # the following helper functions support the default implementation
    # but are not part of the interface that a user needs to implement

    #-------------------------------------------------------------------------
    def GetScripts(self):
        """
        This function returns a dictionary of rendering scripts
        whose key is a descriptive string. This dictionary will
        be used when our  function returns a list of scripts
        to run to access the script source code. The source is
        given to VisIt CLI for execution.

        This implementation expects that the user will have called
        AddScripts for each script, initializing the key and
        script scource.
        """
        # return the disctionary
        if self._Scripts is None:
            return {}
        return self._Scripts

    #-------------------------------------------------------------------------
    def AddScript(self, key, sourceFile='', sourceString='', scriptType='cli'):
        """
        Append a script to the dictionary. This should be called
        any number of times before Initialize runs. the script may
        be provided in a file via sourceFile argument, or in a string
        via sourceString argument. the scriptType may be 'cli' or 'sim'
        to indicate which interpreter will process it. 'cli' scripts
        are executed in serial on VisIt's CLI while 'sim' scripts are
        executed in parallel the simulation interpreter.
        """
        if self._Scripts is None:
            self._Scripts = {}

        rawSource = []

        # sourceString from a file
        if sourceFile:
            if not self._ScriptDir is None:
                sourceFile = os.path.join(self._ScriptDir,sourceFile)
            f = open(sourceFile)
            src = f.read()
            try:
                __import__('compiler').parse(src)
            except:
                pError('Syntax error in %s'%(sourceFile))
                raise
            rawSource = src.split('\n')
            f.close()

        # sourceString from a string
        if sourceString:
            rawSource = sourceString.split('\n')

        # add a header and footer and indent because the
        # engine will wrap the script in a try catch block
        wrappedSource  = '    pStatus(\'Started execution %s : %s\')\n'%(key, sourceFile)
        for l in rawSource:
            wrappedSource += '    ' + l + '\n'
        wrappedSource += '    pStatus(\'Finished execution %s : %s\')\n'%(key, sourceFile)

        self._Scripts[key] = Struct(Source=wrappedSource, Type=scriptType)

        return


    #-------------------------------------------------------------------------
    def SetStartIteration(self, it):
        """
        Set the number of simulation steps to take before visualization
        """
        self._StartIteration = it
        return

    #-------------------------------------------------------------------------
    def SetStopIteration(self, it):
        """
        Set the simulation iteration to stop at.
        """
        self._StopIteration = it
        return

    #-------------------------------------------------------------------------
    def SetStepInterval(self, it):
        """
        Set the number of iterations the simulation advances per update.
        """
        self._Step = it
        return

    #-------------------------------------------------------------------------
    def SetPlotInterval(self, it):
        """
        Set the number of iterations between visualizations.
        """
        self._Plot = it
        return

    #-------------------------------------------------------------------------
    def GetNamespace(self):
        """ Get the warp module that the instance has modified """
        gn = {}
        gn['warp'] = warp
        return gn

    #-------------------------------------------------------------------------
    def WriteMeshHeader(self):
        """ """
        varids = self._IOMeshVars
        root = self._IODir
        header = self._IOMeshHeader
        fname = '%s/%s'%(root, header)
        f = file(fname, 'w')

        dsrc = self._DataSource
        gnx = dsrc.GetMeshSource().GetGlobalSize()

        f.write('# WarpIV BOV Data Writer\n\n')
        f.write('nx=%d, ny=%d, nz=%d\n'%(gnx[0], gnx[1], gnx[2]))
        f.write('dtype=f64\n')
        f.write('ext=warpiv\n\n')

        for varid in varids:

            meshName, varName, srcMeshName, derivedQty, opName = \
                dsrc.DecodeDataSpec(varid)

            if derivedQty:
                if varName:
                    varName = '%s-%s-%s'%(srcMeshName, opName, varName)
                else:
                    varName = '%s-%s'%(srcMeshName, opName)
            else:
                if varName[-1] == 'x':
                    f.write('vector:%s\n'%(varName[0:-1]))

            f.write('scalar:%s\n'%(varName))
        f.close()
        return

    #-------------------------------------------------------------------------
    def WriteMeshData(self):
        """ """
        varids = self._IOMeshVars
        rank = warp.parallel.get_rank()
        dsrc = self._DataSource
        msrc = dsrc.GetMeshSource()
        gext = list(itertools.chain.from_iterable(msrc.GetGlobalExtent()))
        lext = list(itertools.chain.from_iterable(msrc.GetExtent()))
        vext = list(itertools.chain.from_iterable(msrc.GetExtent()))
        vext[1] = max(vext[1] - 1, vext[0]) if vext[1] != gext[1] else vext[1]
        vext[3] = max(vext[3] - 1, vext[2]) if vext[3] != gext[3] else vext[3]
        vext[5] = max(vext[5] - 1, vext[4]) if vext[3] != gext[5] else vext[5]
        it = dsrc.GetTimeStep()
        root = self._IODir
        fname = '%s/%%s_%d.warpiv'%(root, it)

        #pError('varids = %s, gext = %s, lext = %s'%(str(varids), str(gext), str(lext)))

        for varid in varids:

            # parse the request
            meshName, varName, srcMeshName, derivedQty, opName = \
                dsrc.DecodeDataSpec(varid)

            # get requested data
            data = msrc.GetArray(meshName, varName, srcMeshName, derivedQty, opName)

            # fix var name for derived qty's
            if derivedQty:
                if varName:
                    varName = '%s-%s-%s'%(srcMeshName, opName, varName)
                else:
                    varName = '%s-%s'%(srcMeshName, opName)

            # write it to disk
            if writeArray(fname%(varName), gext, lext, vext, data):
                pError('Failed to write "%s" at iteration "%d"'%(varName, it))

            pStatus('Wrote %s'%(varid))
        return


    #-------------------------------------------------------------------------
    def WriteParticleData(self, root, speciesNames, fieldNames):
        it = self._DataSource.GetTimeStep()
        rank = warp.parallel.get_rank()
        itdir = '%d'%(it)
        odir = os.path.join(root,itdir)
        # rank 0 to make dir for this step, other will wait
        # until it exists
        try:
            while not (os.path.exists(odir) and os.path.isdir(odir)):
                if rank == 0:
                    os.mkdir(odir)
                else:
                    pass
        except Exception as e:
            raise
        # write the dataset
        for spec in speciesNames:
            # rank 0 to make the .visit file
            if rank == 0:
                nRanks = warp.parallel.number_of_PE()
                vfn = os.path.join(root,'%s.visit'%(spec))
                new = not os.path.isfile(vfn)
                vf = open(vfn, 'a')
                if new:
                    vf.write('!NBLOCKS %d\n'%(nRanks))
                i = 0
                while i < nRanks:
                    vf.write(os.path.join(itdir,'%s_%d.vtk\n'%(spec,i)))
                    i += 1
                vf.close()
            # all write set of vtk files for this iteration
            fn = os.path.join(odir, '%s_%d.vtk'%(spec,rank))
            f = open(fn, 'wb')
            f.write("# vtk DataFile Version 3.0\n"
                "WarpIV Data\n"
                "BINARY\n"
                "DATASET POLYDATA\n")
            species = self._DataSource.GetSpecies(spec)
            x = species.getx(gather=0)
            y = species.gety(gather=0)
            z = species.getz(gather=0)
            xyz = np.zeros(3*x.size,'d')
            xyz[0::3] = x
            xyz[1::3] = y
            xyz[2::3] = z
            f.write('POINTS %d double\n'%(x.size))
            f.write(xyz.byteswap())
            f.write('\nVERTICES %d %d\n'%(1, x.size+1))
            cells = np.zeros(x.size+1,'i')
            cells[0] = x.size
            cells[1:] = xrange(0, x.size)
            f.write(cells.byteswap())
            f.write('\nPOINT_DATA %d\n'%(x.size))
            f.write('FIELD arrays %d'%(len(fieldNames)))
            for field in fieldNames:
                f.write('\n%s 1 %d double\n'%(field, x.size))
                getFunc = getattr(species,'get' + field)
                f.write(getFunc(gather=0).byteswap())
            f.write('\n')
            f.close()
            pStatus('Wrote %s'%(spec))
