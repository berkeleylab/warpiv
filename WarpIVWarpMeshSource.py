import numpy as np
import warpoptions
warpoptions.ignoreUnknownArgs = True
warpoptions.quietImport = True
warpoptions.init_parser()
import warp
import parallel
from WarpIVUtil import pError, pDebug
import WarpIVYeeCell
import WarpIVPointMesh

# VisIt has a nice feature that if you use
# / in a variable name you can construct nested
# menus and organize variables by mesh.
varsep = '/'

class WarpIVWarpMeshSource(object):
    """
    Data API class for mesh based objects. The data API generalizes
    the interface needed for the vis backend to access the data. This
    implementation handles mesh based data from Warp.
    """
    #-----------------------------------------------------------------------------
    def __init__(self, particleSource):
        """
        construct the mesh source. note a particle source is required
        for derived quantities.
        """
        self._ParticleSource = particleSource
        return

    #-----------------------------------------------------------------------------
    def GetNumberOfDomains(self):
        """ return the number of domains total """
        return parallel.number_of_PE()

    #-----------------------------------------------------------------------------
    def GetDomainIds(self):
        """return the list of domain ids local to this rank """
        return [parallel.get_rank()]

    #-----------------------------------------------------------------------------
    def GetGlobalSize(self, cells=False):
        """return the size of the entire mesh"""
        decomp = self._GetDecomp()
        i = 0 if cells else 1
        return [decomp.nxglobal+i, decomp.nyglobal+i, decomp.nzglobal+i]

    #-----------------------------------------------------------------------------
    def GetGlobalExtent(self, cells=False):
        """ """

        solver = (warp.getregisteredsolver() or warp.w3d)

        gnx = self.GetGlobalSize(cells)

        ext = [[], [], []]
        ext[0] = [0, max(0, gnx[0]-1)]
        ext[1] = [0, 0] if solver.l_2dxz else [0, max(0, gnx[1]-1)]
        ext[2] = [0, max(0, gnx[2]-1)]

        return ext

    #-----------------------------------------------------------------------------
    def GetCoordinates(self, cells=False):
        """
        Get the coordinates as if the grid were a stretched mesh.
        This is a fundemental flaw in VisIt - it can't handle uniform
        meshes.
        """
        x0 = self.GetOrigin()
        dx = self.GetSpacing()
        coords = self.GetIndices()
        i = 0
        while i<3:
            coords[i] *= dx[i]
            coords[i] += x0[i]
            i += 1
        return coords

    #-----------------------------------------------------------------------------
    def GetArrayNames(self):
        """
        """
        varnames = []

        # mesh based arrays
        varmd = warp.getregisteredsolver().dict_of_grids
        for var,var_props in varmd.iteritems():
            varname = 'grid%s%s'%(varsep,var)
            varnames.append(varname)

        # domain decomp
        varname = 'grid%s%s'%(varsep, 'MPI_rank')
        varnames.append(varname)

        # derived quantities
        pMeshNames = self._ParticleSource.GetMeshNames()
        pVarNames = self._ParticleSource.GetArrayNames()
        for pMeshName in pMeshNames:
            varname = 'grid%s%s%sden'%(varsep,pMeshName,varsep)
            varnames.append(varname)
            for var in pVarNames:
                ops = ['min','max','avg']
                for op in ops:
                    varname = 'grid%s%s%s%s%s%s'%(varsep,pMeshName,varsep,op,varsep,var)
                    varnames.append(varname)
        return varnames

    #-----------------------------------------------------------------------------
    def GetArray(self, meshName, varName, srcMeshName, derivedQty, opName):
        """
        Get an array of data by name
        """
        outdim = self.GetSize()

        # mpi rank is a special case we added for debugging
        if varName == 'MPI_rank':
            rank = np.empty(outdim, dtype=float, order='F')
            rank.fill(float(parallel.get_rank()))
            return rank

        if derivedQty:
            species = self._ParticleSource.GetSpecies(srcMeshName)

            solver = warp.getregisteredsolver()

            x = species.getx(gather=0)
            y = np.zeros(x.size, x.dtype) \
                if solver.l_2dxz else species.gety(gather=0)
            z = species.getz(gather=0)

            dx = self.GetSpacing()
            x0 = self.GetLocalOrigin()
            ng = self.GetNumGhosts()

            nci = self.GetSize(False,True)
            ncci = nci[0]*nci[1]*nci[2]
            nco = self.GetSize(False,False)
            ncco = nco[0]*nco[1]*nco[2]

            ids = WarpIVPointMesh.xzMeshIds(x,z, x0[0],x0[2], dx[0],dx[2], nci[0],nci[2]) \
                if solver.l_2dxz else WarpIVPointMesh.meshIds(x,y,z,
                        x0[0],x0[1],x0[2], dx[0],dx[1],dx[2], nci[0],nci[1],nci[2])

            if opName == 'den':
                varName = 'den'
                dq = WarpIVPointMesh.meshDensity(ids, ncci, x.dtype.num)

            else:
                getFunc = getattr(species,'get' + varName)
                if opName == 'avg':
                    den = WarpIVPointMesh.meshDensity(ids, ncci, x.dtype.num)
                    dq = WarpIVPointMesh.meshAverage(getFunc(gather=0), ids, den)

                elif opName == 'min':
                    dq = WarpIVPointMesh.meshMin(getFunc(gather=0), ids, ncci)

                elif opName == 'max':
                    dq = WarpIVPointMesh.meshMax(getFunc(gather=0), ids, ncci)

                else:
                    pError('Invalid op %s'%(opName))
                    return None

            onCells = True
            if onCells:
                varout = WarpIVYeeCell.cpy2(dq, nci[0], nci[2], nco[0], nco[2], ng[0], ng[2]) \
                    if solver.l_2dxz else WarpIVYeeCell.cpy3(dq, nci[0], nci[1], nci[2], \
                        nco[0], nco[1], nco[2], ng[0], ng[1], ng[2])
            else:
                # TODO -- cell to node
                pass

            return varout

        else:
            # get solver specific metadata
            solver = warp.getregisteredsolver()
            varmd = solver.dict_of_grids[varName]
            varget = getattr(solver, varmd['getter'])
            varcen = varmd['centering']

            # nodal data is the easiest
            if varcen == 'node':
                return varget()

            # Multi Solver
            #if isinstance(solver, warp.multigrid.Multi3D):
            #    if varcen == 'node':
            #        # nothing to do
            #        return varget()

            #  EM Solver
            if isinstance(solver, warp.field_solvers.em3dsolver.EM3D):

                ng = [solver.nxguard, solver.nyguard, solver.nzguard]

                if solver.l_2dxz:
                    if varcen == 'Yee':
                        # em.l_2dxz case Ex, Ez and Bx, Bz on are edges. Ey is on
                        # nodes, By is on centers and J has same layout as E
                        # arrays all have size n_q = nv_q + 2*ng_q + 1 where q is
                        # one of x,y,z
                        varin = varget(1, 0)
                        indim = varin.shape

                        if not np.isfortran(varin):
                            pError('Data is expected in FORTRAN order')

                        if varName == 'Bx':
                            # bx is on quad z face
                            varout = WarpIVYeeCell.zAvg2(varin, indim[0], indim[2], \
                                outdim[0], outdim[2], ng[0], ng[2]);
                            return varout

                        if varName == 'By':
                            # by is on quad centers
                            varout = WarpIVYeeCell.xzAvg2(varin, indim[0], indim[2], \
                                outdim[0], outdim[2], ng[0], ng[2]);
                            return varout

                        if varName == 'Bz':
                            # bz is on quad x face
                            varout = WarpIVYeeCell.xAvg2(varin, indim[0], indim[2], \
                                outdim[0], outdim[2], ng[0], ng[2])
                            return varout

                        if varName == 'Ex' or varName == 'Jx':
                            # ex and jx are on quad x edges
                            varout = WarpIVYeeCell.xAvg2(varin, indim[0], indim[2], \
                                outdim[0], outdim[2], ng[0], ng[2]);
                            return varout

                        if varName == 'Ez' or varName == 'Jz':
                            # ez and jz are on quad z edges
                            varout = WarpIVYeeCell.xAvg2(varin, indim[0], indim[2], \
                                outdim[0], outdim[2], ng[0], ng[2]);
                            return varout

                        if varName == 'Ey' or varName == 'Jy':
                            # ey and jy are on quad corners
                            varout = WarpIVYeeCell.cpy2(varin, indim[0], indim[2], \
                                outdim[0], outdim[2], ng[0], ng[2]);
                            return varout

                    elif varcen == 'cell':
                        # move from cell to nodes
                        varout = WarpIVYeeCell.xzAvg2(varin, indim[0], indim[2], \
                            outdim[0], outdim[2], ng[0], ng[2]);
                        return varout

                elif solver.l_1dz:
                    pError('layout \'1dz\' not implemented, treating as node centered')
                    return varget(0, 0)

                elif solver.l_2drz:
                    pError('layout \'2drz\' not implemented, treating as node centered')
                    return varget(0, 0)

                else:
                    if varcen == 'Yee':
                        # 3d case, E and J are on edges B is on faces
                        # arrays all have size n_q = nv_q + 2*ng_q + 1 where q is
                        # one of x,y,z
                        varin = varget(1, 0)
                        indim = varin.shape
                        if not np.isfortran(varin):
                            pError('Data is expected in FORTRAN order')

                        if varName == 'Ex' or varName == 'Jx':
                            # on x edges
                            varout = WarpIVYeeCell.xAvg3(varin, indim[0], indim[1], indim[2], \
                                outdim[0], outdim[1], outdim[2], ng[0], ng[1], ng[2])
                            return varout

                        elif varName == 'Ey' or varName == 'Jy':
                            # on y edges
                            varout = WarpIVYeeCell.yAvg3(varin, indim[0], indim[1], indim[2], \
                                outdim[0], outdim[1], outdim[2], ng[0], ng[1], ng[2])
                            return varout

                        elif varName == 'Ez' or varName == 'Jz':
                            # on z edges
                            varout = WarpIVYeeCell.zAvg3(varin, indim[0], indim[1], indim[2], \
                                outdim[0], outdim[1], outdim[2], ng[0], ng[1], ng[2])
                            return varout

                        elif varName == 'Bx':
                            # bx is on hex yz face
                            varout = WarpIVYeeCell.yzAvg3(varin, indim[0], indim[1], indim[2], \
                                outdim[0], outdim[1], outdim[2], ng[0], ng[1], ng[2])
                            return varout

                        elif varName == 'By':
                            # by is on hex xz face
                            varout = WarpIVYeeCell.xzAvg3(varin, indim[0], indim[1], indim[2], \
                                outdim[0], outdim[1], outdim[2], ng[0], ng[1], ng[2])
                            return varout

                        elif varName == 'Bz':
                            # bz is on hex xy face
                            varout = WarpIVYeeCell.xyAvg3(varin, indim[0], indim[1], indim[2], \
                                outdim[0], outdim[1], outdim[2], ng[0], ng[1], ng[2])
                            return varout

                    elif varcen == 'cell':
                        # move from cell to nodes
                        varout = WarpIVYeeCell.xyzAvg3(varin, indim[0], indim[1], indim[2], \
                            outdim[0], outdim[1], outdim[2], ng[0], ng[1], ng[2])
                        return varout

                    else:
                        # node centered
                        return varget(0, 0)

            else:
                pError('unkown solver %s, treating as node centered'%(type(solver)))
                return varget(0, 0)

    #-----------------------------------------------------------------------------
    def GetNumGhosts(self):
        """Get the number of ghsot zones from warp """
        solver = (warp.getregisteredsolver() or warp.w3d)
        ng = [solver.nxguard, solver.nyguard, solver.nzguard]
        return ng

    #-----------------------------------------------------------------------------
    def _GetDecomp(self):
        """Get the domain decomposition from Warp"""
        solver = (warp.getregisteredsolver() or warp.w3d)
        if solver == warp.w3d:
            decomp = warp.top.fsdecomp
        else:
            decomp = solver.fsdecomp
        return decomp

    #-----------------------------------------------------------------------------
    def GetSize(self, cells=False, ghost=False):
        """
        Get this rank's grid size in units of cells
        (or units of points if cells is False).
        """
        decomp = self._GetDecomp()

        i = 0 if cells else 1
        ng = self.GetNumGhosts() if ghost else [0, 0, 0]

        size = [[],[],[]]
        size[0] = max(1, decomp.nx[decomp.ixproc]+i+2*ng[0])
        size[1] = max(1, decomp.ny[decomp.iyproc]+i+2*ng[1])
        size[2] = max(1, decomp.nz[decomp.izproc]+i+2*ng[2])

        if __debug__: pDebug(size)
        return size

    #-----------------------------------------------------------------------------
    def GetExtent(self, cells=False, ghost=False):
        """
        Get this rank's index space grid extent in units of cells
        (or units of points if cells is False).
        """
        solver = (warp.getregisteredsolver() or warp.w3d)
        decomp = self._GetDecomp()

        i = 1 if cells else 0
        ng = self.GetNumGhosts() if ghost else [0, 0, 0]

        ext = [[],[],[]]
        ext[0] = [decomp.ix[decomp.ixproc]-ng[0], decomp.ix[decomp.ixproc]+decomp.nx[decomp.ixproc]-i+ng[0]]
        ext[1] = [0, 0] if solver.l_2dxz else \
            [decomp.iy[decomp.iyproc]-ng[1], decomp.iy[decomp.iyproc]+decomp.ny[decomp.iyproc]-i+ng[1]]
        ext[2] = [decomp.iz[decomp.izproc]-ng[2], decomp.iz[decomp.izproc]+decomp.nz[decomp.izproc]-i+ng[2]]

        return ext

    #-----------------------------------------------------------------------------
    def GetIndices(self, cells=False):
        """
        Get this rank's grid indices in units of cells
        (or units of points if cells is False).
        """
        solver = (warp.getregisteredsolver() or warp.w3d)
        decomp = self._GetDecomp()

        i = 0 if cells else 1

        ids = []
        ids.append(np.arange(decomp.ix[decomp.ixproc], decomp.ix[decomp.ixproc]+decomp.nx[decomp.ixproc]+i, dtype=np.float64))
        ids.append(np.zeros(1, dtype=np.float64) if solver.l_2dxz else  \
            np.arange(decomp.iy[decomp.iyproc], decomp.iy[decomp.iyproc]+decomp.ny[decomp.iyproc]+i, dtype=np.float64))
        ids.append(np.arange(decomp.iz[decomp.izproc], decomp.iz[decomp.izproc]+decomp.nz[decomp.izproc]+i, dtype=np.float64))

        return ids

    #-----------------------------------------------------------------------------
    def GetSpacing(self):
        """Get the grid cell spacing"""
        solver = (warp.getregisteredsolver() or warp.w3d)
        dx = [[],[],[]]
        dx[0] = warp.w3d.dx
        dx[1] = 1.0 if solver.l_2dxz else warp.w3d.dy
        dx[2] = warp.w3d.dz
        return dx

    #-----------------------------------------------------------------------------
    def GetOrigin(self):
        """Get the lower left corner of the grid"""
        solver = (warp.getregisteredsolver() or warp.w3d)
        x0 = [[],[],[]]
        x0[0] = warp.w3d.xmmin
        x0[1] = 0.0 if solver.l_2dxz else warp.w3d.ymmin
        x0[2] = warp.w3d.zmmin
        return x0

    #-----------------------------------------------------------------------------
    def GetLocalOrigin(self, cells=False):
        """Get the origin of the grid on this process"""

        solver = (warp.getregisteredsolver() or warp.w3d)
        dx = self.GetSpacing()
        x0 = self.GetOrigin()
        ext = self.GetExtent(False, True)

        lx0 = [[],[],[]]
        lx0[0] = x0[0] + dx[0]*ext[0][0]
        lx0[1] = 0.0 if solver.l_2dxz else x0[1] + dx[1]*ext[1][0]
        lx0[2] = x0[2] + dx[2]*ext[2][0]

        return lx0
