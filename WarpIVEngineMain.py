#!/bin/python
import os
import sys
# hide command line from warp
import warpoptions
warpoptions.ignoreUnknownArgs = True
warpoptions.quietImport = True
warpoptions.init_parser()

import argparse
import time
from WarpIVUtil import VisItEnv
env = VisItEnv()
from WarpIVUtil import pError,pDebug,pStatus
from WarpIVEngine import WarpIVEngine
from WarpIVSimulation import WarpIVSimulation
from WarpIVSimulationFactory import WarpIVSimulationFactory

#-----------------------------------------------------------------------------
if __name__ == "__main__":
    pStatus('WarpIVEngineMain started')

    # parse command line args
    ap = argparse.ArgumentParser(usage=argparse.SUPPRESS,prog='',add_help=False)
    ap.add_argument('--help',default=False,action='store_true')
    opts = vars(ap.parse_known_args(sys.argv)[0])
    if opts['help']:
        pStatus('\nWarpIVEngineMain\nUsage:\n\nWarp\n%s\nWarpIVEngine\n%s\nWarpIVSimulation\n%s\nWarpIVSimulationFactory\n%s'%(
            warpoptions.warpoptionsstr(),
            WarpIVEngine.GetCommandLineHelp(),
            WarpIVSimulation.GetCommandLineHelp(),
            WarpIVSimulationFactory.GetCommandLineHelp(sys.argv)))
        sys.exit(0)

    # use the factory to create user's simualtion
    factory = WarpIVSimulationFactory(sys.argv)
    simulation = factory.CreateSimulation()
    simulation.Initialize()

    # create a visit engine
    engine = WarpIVEngine(sys.argv)
    engine.SetSimulation(simulation)
    engine.Initalize()

    # run
    status = engine.EventLoop()

    # shut down engine and simulation
    engine.Finalize()
    simulation.Finalize()

    pStatus('WarpIVEngineMain finished with status %d'%(status))
    sys.exit(0)
